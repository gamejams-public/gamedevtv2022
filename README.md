# Game Dev.TV Game Jam 2022

## Info

Game built with Unreal Engine 5

## Set up

### Git LFS

Install if you haven't already:

```shell
git lfs install
```

Track new files in Git LFS that don't match the initial `.gitattributes` pattern with

```shell
git lfs track '<pattern>'
```


## Releasing

On main run the following with the new version under the game directory:

```
./release_version 1.0.0
```
