// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "BaseCharacter.h"

#include <deque>

#include "PurgatoryCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
class UAnimMontage;
class USoundBase;
class URighteousMeterComponent;

UCLASS(config=Game)
class APurgatoryCharacter : public ABaseCharacter
{
	GENERATED_BODY()

public:
	APurgatoryCharacter();

	virtual void PlayFireWeaponAnimation(UAnimMontage* FireWeaponAnimationMontage) override;
	virtual void AttachWeapon(UTP_WeaponComponent* WeaponComponent) override;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float TurnRateGamepad;

	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	virtual void Jump() override;

	virtual void Tick(float DeltaTime) override;

	const URighteousMeterComponent* GetRighteousMeterComponent() const { return bRighteousComponentEnabled ? RighteousMeterComponent: nullptr; }
	URighteousMeterComponent* GetRighteousMeterComponent() { return bRighteousComponentEnabled ? RighteousMeterComponent : nullptr; }

protected:

	virtual void FellOutOfWorld(const class UDamageType& dmgType) override;
	virtual void OnHurt(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator) override;

	virtual void BeginPlay() override;

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles strafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	struct TouchData
	{
		TouchData() { bIsPressed = false;Location=FVector::ZeroVector;}
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};

	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);

	TouchData	TouchItem;

	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	/* 
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so 
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */
	bool EnableTouchscreenMovement(UInputComponent* InputComponent);

private:
	void CheckMovementNoise();

	float GetMovementSpeedPercent() const;

	UFUNCTION()
	void OnActorOverlap(AActor* OverlappedActor, AActor* OtherActor);

	void OnOverlappedEnemy(APawn* Enemy);

private:
	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* Mesh1P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float MovementPercentNoiseThreshold{ 0.5f };

	UPROPERTY(EditAnywhere, Category = "Movement")
	float MovementSamplingDeltaTime{ 0.1f };

	UPROPERTY(Category = "Gameplay", VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class URighteousMeterComponent* RighteousMeterComponent{};

	float LastMovementSampleWorldTimeSeconds{};

	std::deque<float> MovementPercentSamples;

	inline static constexpr std::size_t MaxMovementSamples = 30;

	bool bRighteousComponentEnabled{};
};

