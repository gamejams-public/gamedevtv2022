// Copyright Epic Games, Inc. All Rights Reserved.

#include "PurgatoryCharacter.h"
#include "PurgatoryProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "RighteousMeterComponent.h"

#include "Logging.h"
#include "Noise.h"

#include <numeric>

#include "TP_WeaponComponent.h"


//////////////////////////////////////////////////////////////////////////
// APurgatoryCharacter

APurgatoryCharacter::APurgatoryCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	TurnRateGamepad = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	RighteousMeterComponent = CreateDefaultSubobject<URighteousMeterComponent>(TEXT("RighteousMeterComponent"));

}

void APurgatoryCharacter::PlayFireWeaponAnimation(UAnimMontage* FireWeaponAnimationMontage)
{
	// Try and play a firing animation if specified
	if (FireWeaponAnimationMontage != nullptr)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = GetMesh1P()->GetAnimInstance();
		if (AnimInstance != nullptr)
		{
			AnimInstance->Montage_Play(FireWeaponAnimationMontage, 1.f);
		}
	}
}

void APurgatoryCharacter::AttachWeapon(UTP_WeaponComponent* WeaponComponent)
{
	Super::AttachWeapon(WeaponComponent);

	if (!WeaponComponent)
	{
		UE_LOG(LogPlayer, Warning, TEXT("%s: Attempted to attach null UTP_WeaponComponent!"), *GetName());
		return;
	}

	// Attach the weapon to the First Person Character
	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
	WeaponComponent->GetOwner()->AttachToComponent(GetMesh1P(), AttachmentRules, FName(TEXT("GripPoint")));
}

void APurgatoryCharacter::Jump()
{
	Super::Jump();

	MakeNoise(Noise::Jump, GetActorLocation());
}

void APurgatoryCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CheckMovementNoise();
}

void APurgatoryCharacter::OnOverlappedEnemy(APawn* Enemy)
{
	if (!Enemy)
	{
		return;
	}

	UE_LOG(LogPlayer, Log, TEXT("%s: Bumped into AI enemy %s"), *GetName(), *Enemy->GetName());
	MakeNoise(Noise::Movement, GetActorLocation());
}

void APurgatoryCharacter::FellOutOfWorld(const UDamageType& dmgType)
{
	if (bRighteousComponentEnabled)
	{
		RighteousMeterComponent->Suicide();
	}

	Super::FellOutOfWorld(dmgType);
}

void APurgatoryCharacter::OnHurt(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator)
{
	Super::OnHurt(CurrentHealth, PreviousHealth, DamageOwner, DamageCauser, DamageInstigator);

	if (bRighteousComponentEnabled && DamageInstigator)
	{
		RighteousMeterComponent->HurtBy(DamageInstigator->GetPawn());
	}
}

void APurgatoryCharacter::BeginPlay()
{
	Super::BeginPlay();

	SpawnAttachedStartingWeapon();

	// TODO: Only enable this for purgatory level - can check this on game instance
	bRighteousComponentEnabled = RighteousMeterComponent != nullptr;

	OnActorBeginOverlap.AddDynamic(this, &APurgatoryCharacter::OnActorOverlap);
}

//////////////////////////////////////////////////////////////////////////// Input

void APurgatoryCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &APurgatoryCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("PrimaryAction", IE_Pressed, this, &APurgatoryCharacter::FireWeapon);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	// Bind movement events
	PlayerInputComponent->BindAxis("Move Forward / Backward", this, &APurgatoryCharacter::MoveForward);
	PlayerInputComponent->BindAxis("Move Right / Left", this, &APurgatoryCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "Mouse" versions handle devices that provide an absolute delta, such as a mouse.
	// "Gamepad" versions are for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn Right / Left Mouse", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("Look Up / Down Mouse", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn Right / Left Gamepad", this, &APurgatoryCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("Look Up / Down Gamepad", this, &APurgatoryCharacter::LookUpAtRate);
}

void APurgatoryCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		FireWeapon();
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void APurgatoryCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
}

void APurgatoryCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void APurgatoryCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void APurgatoryCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}

void APurgatoryCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}

bool APurgatoryCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &APurgatoryCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &APurgatoryCharacter::EndTouch);

		return true;
	}
	
	return false;
}

void APurgatoryCharacter::CheckMovementNoise()
{
	auto World = GetWorld();
	if (!World)
	{
		return;
	}

	const float CurrentTime = World->GetTimeSeconds();
	float DeltaTime = CurrentTime - LastMovementSampleWorldTimeSeconds;

	if (DeltaTime < MovementSamplingDeltaTime)
	{
		return;
	}

	LastMovementSampleWorldTimeSeconds = CurrentTime;

	if (MovementPercentSamples.size() == MaxMovementSamples)
	{
		MovementPercentSamples.pop_back();
	}

	MovementPercentSamples.push_front(GetMovementSpeedPercent());

	auto MovementSpeedAverage = std::accumulate(MovementPercentSamples.begin(), MovementPercentSamples.end(), 0.0f) / MovementPercentSamples.size();

	if (MovementSpeedAverage >= MovementPercentNoiseThreshold)
	{
		UE_LOG(LogPlayer, VeryVerbose, TEXT("%s: Trigger noise threshold with movement - MaxRatioAverage = %f >= %f"),
			*GetName(), MovementSpeedAverage, MovementPercentNoiseThreshold);

		MakeNoise(Noise::Movement, GetActorLocation());
	}
	else if (UE_LOG_ACTIVE(LogPlayer, VeryVerbose))
	{
		UE_LOG(LogPlayer, VeryVerbose, TEXT("%s: Did not trigger noise threshold with movement - MaxRatioAverage = %f < %f"),
			*GetName(), MovementSpeedAverage, MovementPercentNoiseThreshold);
	}
}

float APurgatoryCharacter::GetMovementSpeedPercent() const
{
	auto Movement = GetCharacterMovement();

	if (!Movement)
	{
		return 0.0f;
	}

	auto MaxSpeed = Movement->GetMaxSpeed();

	if (FMath::IsNearlyZero(MaxSpeed))
	{
		return 0.0f;
	}

	auto CurrentSpeed = GetVelocity().Size();


	return CurrentSpeed / MaxSpeed;
}

void APurgatoryCharacter::OnActorOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	APawn* OtherPawn = Cast<APawn>(OtherActor);
	if (!OtherPawn)
	{
		return;
	}

	auto OtherController = OtherPawn->GetController();
	if (!OtherController || OtherController->IsPlayerController())
	{
		return;
	}


	OnOverlappedEnemy(OtherPawn);
}
