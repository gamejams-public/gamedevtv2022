// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Damageable.h"

#include "BaseCharacter.generated.h"

class UAnimMontage;
class UTP_WeaponComponent;
class AWeapon;
class UHealthComponent;
class USoundBase;

namespace Noise
{
	struct FNoiseConfig;
}

// Declaration of the delegate that will be called when the Primary Action is triggered
// It is declared as dynamic so it can be accessed also in Blueprints
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUseItem);

UCLASS(Abstract)
class PURGATORY_API ABaseCharacter : public ACharacter, public IDamageable
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();

	/** Fires a projectile. */
	UFUNCTION(BlueprintCallable)
	virtual void FireWeapon();

	UFUNCTION(BlueprintPure)
	bool HasWeapon() const;

	UFUNCTION(BlueprintCallable)
	void EquipStartingWeapon();

	virtual void AttachWeapon(UTP_WeaponComponent* WeaponComponent);
	virtual void DetachWeapon(UTP_WeaponComponent* WeaponComponent);

	virtual void PlayFireWeaponAnimation(UAnimMontage* FireWeaponAnimationMontage) PURE_VIRTUAL(, );

	UFUNCTION(BlueprintPure)
	bool IsDead() const;

	UFUNCTION(BlueprintPure)
	bool NotDead() const;

	const UHealthComponent* GetHealthComponent() const;

	UHealthComponent* GetHealthComponent();

	/** Delegate to whom anyone can subscribe to receive this event */
	UPROPERTY(BlueprintAssignable, Category = "Interaction")
	FOnUseItem OnUseItem;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void SpawnAttachedStartingWeapon();

	/*
	* Fell through Kill-Z volume
	* @see https://docs.unrealengine.com/4.27/en-US/Basics/Actors/Volumes/#Volume%20Types
	*/
	virtual void FellOutOfWorld(const class UDamageType& dmgType) override;

	void MakeNoise(const Noise::FNoiseConfig& NoiseConfig, const FVector& Location);

	// Inherited via IDamageable
	virtual void OnInit(float Health, float MaxHealth) override;
	virtual void OnAnyDamage(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator) override;
	virtual void OnGainedHealth(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator) override;
	virtual void OnHurt(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator) override;
	virtual void OnKilled(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator) override;

	void PlaySoundAtActorLocation(USoundBase* Sound);

private:
	void Killed();

private:

	UPROPERTY(Category = "Weapons", EditDefaultsOnly)
	TSubclassOf<AWeapon> StartingWeaponClass{};

	UPROPERTY(Category = "Health", VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UHealthComponent* HealthComponent {};

	bool bDead{};
	bool bHasWeapon{};
};

inline const UHealthComponent* ABaseCharacter::GetHealthComponent() const
{
	check(HealthComponent);
	return HealthComponent;
}

inline UHealthComponent* ABaseCharacter::GetHealthComponent()
{
	check(HealthComponent);
	return HealthComponent;
}

inline bool ABaseCharacter::IsDead() const
{
	return bDead;
}

inline bool ABaseCharacter::NotDead() const
{
	return !IsDead();
}

inline bool ABaseCharacter::HasWeapon() const
{
	return bHasWeapon;
}
