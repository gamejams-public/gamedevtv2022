// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PurgatoryGameStateBase.h"
#include "HeavenGameState.generated.h"

/**
 * 
 */
UCLASS()
class PURGATORY_API AHeavenGameState : public APurgatoryGameStateBase
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure)
	int32 GetCurrentQuestionIndex() const;

	UFUNCTION(BlueprintPure)
	int32 GetTotalQuestionCount() const;

private:

	friend class AHeavenGameMode;

	int32 CurrentQuestionIndex{};
	int32 TotalQuestions{};
};

inline int32 AHeavenGameState::GetCurrentQuestionIndex() const
{
	return CurrentQuestionIndex;
}

inline int32 AHeavenGameState::GetTotalQuestionCount() const
{
	return TotalQuestions;
}
