// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "Damageable.h"

#include "Logging.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;

	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::TakeDamage);

	IDamageable* DamageableActor = Cast<IDamageable>(GetOwner());

	if (DamageableActor)
	{
		DamageableActor->OnInit(Health, MaxHealth);
	}
}


void UHealthComponent::TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	const float PreviousHealth = Health;

	// Clamp health to MaxHealth on max in case actor picked up a health pickup
	const float DamageApplied = FMath::Clamp(Damage, Health - MaxHealth, Health);

	Health -= DamageApplied;

	AActor* DamageCauserOwner = DamageCauser->GetOwner();

	UE_LOG(LogPawn, Log, TEXT("%s: Damaged Actor %s by %s owned by %s - Damage=%f;DamageApplied=%f;HealthRemaining=%f"),
		*GetName(),
		*DamagedActor->GetName(),
		*DamageCauser->GetName(),
		(DamageCauserOwner ? *DamageCauserOwner->GetName() : TEXT("NULL")),
		Damage,
		DamageApplied,
		Health);

	IDamageable* DamageableActor = Cast<IDamageable>(DamagedActor);

	if (!DamageableActor)
	{
		UE_LOG(LogPawn, Warning, TEXT("%s: %s is not Damageable - will not receive callbacks"),
			*GetName(),
			*DamagedActor->GetName()
		);

		return;
	}


	if (DamageApplied > 0)
	{
		DamageableActor->OnAnyDamage(Health, PreviousHealth, DamageCauserOwner, DamageCauser, InstigatedBy);

		if (IsDead())
		{
			DamageableActor->OnKilled(Health, PreviousHealth, DamageCauserOwner, DamageCauser, InstigatedBy);
		}
		else
		{
			DamageableActor->OnHurt(Health, PreviousHealth, DamageCauserOwner, DamageCauser, InstigatedBy);
		}
	}
	else if (!FMath::IsNearlyZero(DamageApplied))
	{
		DamageableActor->OnGainedHealth(Health, PreviousHealth, DamageCauserOwner, DamageCauser, InstigatedBy);
	}
}
