// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RighteousMeterComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PURGATORY_API URighteousMeterComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	URighteousMeterComponent();

	UFUNCTION(BlueprintPure)
	float GetRighteousPercent() const;

	UFUNCTION(BlueprintPure)
	bool IsRighteous() const;

	UFUNCTION(BlueprintPure)
	bool IsEvil() const;

	void HurtBy(AActor* Instigator);
	void Hurt(AActor* Instigator);

	void Suicide();


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	int32 RecordConsecutiveHurtByCount(AActor* Actor);
	void ResetConsecutiveHurtByCount(AActor* Actor);

	void UpdateRighteousMeter(float DeltaAmount);

private:
	float RighteousPercent{ 0.0f };

	UPROPERTY(Category = "Righteous", EditDefaultsOnly)
	float MistakePenaltyAmount{ 0.15f };

	UPROPERTY(Category = "Righteous", EditDefaultsOnly)
	float RewardAmount{ 0.1f };

	UPROPERTY(Category = "Righteous", EditDefaultsOnly)
	float ConsecutiveDamagePenalty{ 0.25f };

	UPROPERTY(Category = "Righteous", EditDefaultsOnly)
	int32 ConsecutiveDamageCount{ 5 };

	UPROPERTY()
	TMap<TWeakObjectPtr<AActor>, int32> AggressorActors;

	UPROPERTY()
	TSet<TWeakObjectPtr<AActor>> HurtActors;

	UPROPERTY()
	TSet<TWeakObjectPtr<AActor>> ConsecutiveHitCountPenaltyActors;
};

inline float URighteousMeterComponent::GetRighteousPercent() const
{
	return RighteousPercent;
}

inline bool URighteousMeterComponent::IsRighteous() const
{
	return GetRighteousPercent() >= 0.5f;
}

inline bool URighteousMeterComponent::IsEvil() const
{
	return !IsRighteous();
}
