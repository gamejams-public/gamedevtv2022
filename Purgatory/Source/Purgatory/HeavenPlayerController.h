// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PurgatoryPlayerController.h"
#include "Quiz.h"

#include "HeavenPlayerController.generated.h"

class AHeavenGameMode;

/**
 * 
 */
UCLASS()
class PURGATORY_API AHeavenPlayerController : public APurgatoryPlayerController, public IQuiz
{
	GENERATED_BODY()

public:

	virtual const FQuizQuestion* GetNextQuestion() override;
	virtual EQuizAnswerResult AnswerQuestion(int32 QuestionId, const FString& AnswerValue) override;

protected:
	virtual void TriggerEndOfLevel(bool bIsWinner) override;
	virtual void OnSpectatorPawnActivated(APawn* MySpectatorPawn, APawn* PreviousPlayerPawn) override;

private:
	AHeavenGameMode* GetGameMode() const;

private:
	UPROPERTY(Category = "UI", EditDefaultsOnly)
	TSubclassOf<class UUserWidget> WinClass{};

	bool bWinnerOfLastGame{};
};
