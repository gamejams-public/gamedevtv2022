// Fill out your copyright notice in the Description page of Project Settings.


#include "HeavenGameMode.h"

#include "HeavenGameState.h"
#include "Logging.h"

#include <chrono>
#include <random>
#include <algorithm>

void AHeavenGameMode::BeginPlay()
{
	Super::BeginPlay();

	QuizQuestions.push_back(FQuizQuestion
		{ 1,
			TEXT("What is your quest?"),
			{
				FQuizAnswer { TEXT("A"), TEXT("To beat this game")},
				FQuizAnswer { TEXT("B"), TEXT("To be the best")},
				FQuizAnswer { TEXT("C"), TEXT("To seek the Holy Grail")},
				FQuizAnswer { TEXT("D"), TEXT("Fame and Fortune")}
			},
		TEXT("C")
		});


	QuizQuestions.push_back(FQuizQuestion
		{ 2,
			TEXT("Knowing this game is made in Unreal,\nwhat is the best programming language?"),
			{
				FQuizAnswer { TEXT("A"), TEXT("C#")},
				FQuizAnswer { TEXT("B"), TEXT("Java")},
				FQuizAnswer { TEXT("C"), TEXT("C++")},
				FQuizAnswer { TEXT("D"), TEXT("Python")}
			},
			TEXT("C")
		});

	QuizQuestions.push_back(FQuizQuestion
		{ 3,
			TEXT("What are Quaternions good for?"),
			{
				FQuizAnswer { TEXT("A"), TEXT("Absolutely nothing!")},
				FQuizAnswer { TEXT("B"), TEXT("Rotation Interpolation")},
				FQuizAnswer { TEXT("C"), TEXT("Planar Projections")},
				FQuizAnswer { TEXT("D"), TEXT("Displacement Calculations")}
			},
			TEXT("B")
		});

	QuizQuestions.push_back(FQuizQuestion
		{ 4,
			TEXT("Which of these is a stable sort?"),
			{
				FQuizAnswer { TEXT("A"), TEXT("Quick Sort")},
				FQuizAnswer { TEXT("B"), TEXT("Merge Sort")},
				FQuizAnswer { TEXT("C"), TEXT("Heap Sort")},
				FQuizAnswer { TEXT("D"), TEXT("Selection Sort")}
			},
			TEXT("B")
		});

	QuizQuestions.push_back(FQuizQuestion
		{ 5,
			TEXT("What is the derivative of x^3?"),
			{
				FQuizAnswer { TEXT("A"), TEXT("2*x^2")},
				FQuizAnswer { TEXT("B"), TEXT("1/x^2")},
				FQuizAnswer { TEXT("C"), TEXT("Undefined")},
				FQuizAnswer { TEXT("D"), TEXT("3*x^2")}
			},
			TEXT("D")
		});

	QuizQuestions.push_back(FQuizQuestion
		{ 6,
			TEXT("What are the 3C's of game design?"),
			{
				FQuizAnswer { TEXT("A"), TEXT("Cooking, Cleaning, and Crafting")},
				FQuizAnswer { TEXT("B"), TEXT("Character, Collision, and Coolness")},
				FQuizAnswer { TEXT("C"), TEXT("Camera, Commentary, and Controls")},
				FQuizAnswer { TEXT("D"), TEXT("Character, Camera, and Controls")}
			},
			TEXT("D")
		});

	QuizQuestions.push_back(FQuizQuestion
		{ 7,
			TEXT("Which of the following is NOT a phase\n of game development?"),
			{
				FQuizAnswer { TEXT("A"), TEXT("Ideation")},
				FQuizAnswer { TEXT("B"), TEXT("Preproduction")},
				FQuizAnswer { TEXT("C"), TEXT("Production")},
				FQuizAnswer { TEXT("D"), TEXT("Anxiety")}
			},
			TEXT("D")
		});

	QuizQuestions.push_back(FQuizQuestion
		{ 8,
			TEXT("Which of the following are examples\n of gameplay mechanics?"),
			{
				FQuizAnswer { TEXT("A"), TEXT("Mines")},
				FQuizAnswer { TEXT("B"), TEXT("Moving Platforms")},
				FQuizAnswer { TEXT("C"), TEXT("Floor Switches")},
				FQuizAnswer { TEXT("D"), TEXT("All of the Above")}
			},
			TEXT("D")
		});

	QuizQuestions.push_back(FQuizQuestion
		{ 9,
			TEXT("Which of the following is NOT a way\n to represent a rotation?"),
			{
				FQuizAnswer { TEXT("A"), TEXT("Neo Angles")},
				FQuizAnswer { TEXT("B"), TEXT("Euler Angles")},
				FQuizAnswer { TEXT("C"), TEXT("Quaternions")},
				FQuizAnswer { TEXT("D"), TEXT("Rotation Matrices")}
			},
			TEXT("A")
		});

	QuizQuestions.push_back(FQuizQuestion
		{ 10,
			TEXT("Which of the following is NOT a type of chemical bond?"),
			{
				FQuizAnswer { TEXT("A"), TEXT("Ionic")},
				FQuizAnswer { TEXT("B"), TEXT("Covalent")},
				FQuizAnswer { TEXT("C"), TEXT("Polar")},
				FQuizAnswer { TEXT("D"), TEXT("James Bond")}
			},
			TEXT("D")
		});

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine rng(seed);


	std::shuffle(QuizQuestions.begin(), QuizQuestions.end(), rng);

	NumQuestions = FMath::Min<int32>(NumQuestions, QuizQuestions.size());
}

const FQuizQuestion* AHeavenGameMode::GetNextQuestion()
{
	auto State = GetGameState<AHeavenGameState>();
	if (!State)
	{
		UE_LOG(LogGameMode, Error, TEXT("%s: No game state"), *GetName());
		return nullptr;
	}

	if (!State->CurrentQuestionIndex)
	{
		State->TotalQuestions = NumQuestions;
	}

	if (State->CurrentQuestionIndex == NumQuestions)
	{
		UE_LOG(LogGameMode, Display, TEXT("%s: No more questions"), *GetName());
		return nullptr;
	}

	return &QuizQuestions[State->CurrentQuestionIndex++];
}

EQuizAnswerResult AHeavenGameMode::AnswerQuestion(int32 QuestionId, const FString& AnswerValue)
{
	// TODO: Don't need QuestionId -> Can just use CurrentQuestionIndex - though need to rework how CurrentQuestionIndex is increment in GetNextQuestion

	auto It = std::find_if(QuizQuestions.begin(), QuizQuestions.end(), [QuestionId](const auto& Question) { return Question.QuestionId == QuestionId;  });
	check(It != QuizQuestions.end());

	const auto& Question = *It;

	if (Question.CorrectValue.Equals(AnswerValue, ESearchCase::Type::IgnoreCase))
	{
		UE_LOG(LogGameMode, Log, TEXT("%s: Answered question=%d correctly with answer %s"), *GetName(), QuestionId, *AnswerValue);
		return EQuizAnswerResult::Correct;
	}

	// if bad input entered then let user try again
	auto AnswerExists = Question.Answers.ContainsByPredicate([&AnswerValue](const auto& QuizAnswer)
	{
		return QuizAnswer.Value.Equals(AnswerValue, ESearchCase::Type::IgnoreCase);
	});

	if (!AnswerExists)
	{
		UE_LOG(LogGameMode, Log, TEXT("%s: Answered question=%d with an invalid answer %s - allowing another try"), *GetName(), QuestionId, *AnswerValue);
		return EQuizAnswerResult::BadEntry;
	}

	UE_LOG(LogGameMode, Log, TEXT("%s: Answered question=%d incorrectly with answer %s; CorrectAnswer=%s"),
		*GetName(), QuestionId, *AnswerValue, *Question.CorrectValue);

	// No second chances
	return EQuizAnswerResult::Fail;
}
