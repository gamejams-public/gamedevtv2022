// Fill out your copyright notice in the Description page of Project Settings.


#include "HeavenPlayerController.h"

#include "HeavenGameMode.h"
#include "PurgatoryGameInstance.h"
#include "Logging.h"


AHeavenGameMode* AHeavenPlayerController::GetGameMode() const
{
    auto World = GetWorld();
    if (!World)
    {
        UE_LOG(LogPlayer, Error, TEXT("%s: World is NULL"), *GetName());
        return {};
    }
    auto Mode = Cast<AHeavenGameMode>(World->GetAuthGameMode());
    if (!Mode)
    {
        UE_LOG(LogPlayer, Error, TEXT("%s: Unable to get AHeavenGameMode - called on client in multiplayer?"), *GetName());
        return {};
    }

    return Mode;
}

void AHeavenPlayerController::TriggerEndOfLevel(bool bIsWinner)
{
    this->bWinnerOfLastGame = bIsWinner;

    if(bIsWinner)
    {
        DisplayWidgetClass(WinClass);
    }

    auto PurgatoryGameInstance = Cast<UPurgatoryGameInstance>(GetGameInstance());
    verify(PurgatoryGameInstance);

    PurgatoryGameInstance->CompletedMap(bIsWinner);
}

void AHeavenPlayerController::OnSpectatorPawnActivated(APawn* MySpectatorPawn, APawn* PreviousPlayerPawn)
{
    // TODO: Focus back on ATerminal actor if lose
    if (!bWinnerOfLastGame)
    {
        return;
    }

    if (!PreviousPlayerPawn)
    {
        return;
    }

    const FVector Location(PreviousPlayerPawn->GetActorLocation());
    const FVector FaceDirection((MySpectatorPawn->GetActorLocation() - Location).GetSafeNormal());

    FRotator LookAtRotation = FaceDirection.Rotation();

    MySpectatorPawn->SetActorRotation(LookAtRotation);

    const FVector SpectatorLocation = Location - FaceDirection * 20.0f;

    MySpectatorPawn->SetActorLocation(SpectatorLocation);
}

const FQuizQuestion* AHeavenPlayerController::GetNextQuestion()
{
    auto Mode = GetGameMode();
    if (!Mode)
    {
        return {};
    }

    return Mode->GetNextQuestion();
}

EQuizAnswerResult AHeavenPlayerController::AnswerQuestion(int32 QuestionId, const FString& AnswerValue)
{
    auto Mode = GetGameMode();
    if (!Mode)
    {
        return {};
    }

    return Mode->AnswerQuestion(QuestionId, AnswerValue);
}
