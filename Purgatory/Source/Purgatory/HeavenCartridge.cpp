// Fill out your copyright notice in the Description page of Project Settings.


#include "HeavenCartridge.h"

#include "HeavenPlayerController.h"

#include "Kismet/GameplayStatics.h"
#include "Logging.h"

void UHeavenCartridge::BeginPlay()
{
	Super::BeginPlay();

	PrintLine(TEXT("Before we let you in, you must first pass this test..."));
	PrintLine(TEXT("Press TAB and then enter to begin..."));
}

void UHeavenCartridge::OnInput(const FString& Input)
{
	if (!bStarted)
	{
		ClearScreen();
		PresentQuestion();

		bStarted = true;
	}
	else if (!bGameOver)
	{
		auto AnswerResult = ProcessAnswer(Input);
		switch (AnswerResult)
		{
			case EQuizAnswerResult::Correct:
			{
				ClearScreen();
				PresentQuestion();

				break;
			}
			case EQuizAnswerResult::BadEntry: 
			case EQuizAnswerResult::Incorrect:
			{
				ClearLastLine();

				break;
			}
			case EQuizAnswerResult::Fail:
			{
				bGameOver = true;

				ClearScreen();
				PrintLine("You have failed this quest\nYou must now battle your way back through hell!");

				if (auto World = GetWorld(); World)
				{
					World->GetTimerManager().SetTimer(LostTravelDelayTimer, this, &UHeavenCartridge::TriggerAfterLostGame, LoseTimeTravelDelay);
				}
				else
				{
					UE_LOG(LogPlayer, Error, TEXT("%s: World is NULL"), *GetName());
					TriggerAfterLostGame();
				}

				break;
			}
		}
	}
	else
	{
		UE_LOG(LogPlayer, Log, TEXT("%s: Entered input %s after game over"), *GetName(), *Input);
		ClearLastLine();
	}
}

void UHeavenCartridge::PresentQuestion()
{
	auto PlayerController = GetPlayerController();
	if (!PlayerController)
	{
		return;
	}

	const auto NextQuestion = PlayerController->GetNextQuestion();
	if (!NextQuestion)
	{
		UE_LOG(LogPlayer, Display, TEXT("%s: No more questions - player has won!"), *GetName());
		bGameOver = true;

		ClearScreen();
		PrintLine("You have won!  We grant you access to Heaven");

		PlayerController->GameHasEnded(GetOwner(), true);

		return;
	}

	CurrentQuestionId = NextQuestion->QuestionId;

	PrintLine(*NextQuestion->QuestionText);
	
	for (const auto& Answer : NextQuestion->Answers)
	{
		PrintLine(FString::Printf(TEXT("%s) %s"), *Answer.Value, *Answer.Text));
	}
}

EQuizAnswerResult UHeavenCartridge::ProcessAnswer(const FString& Input)
{
	auto PlayerController = GetPlayerController();
	if (!PlayerController)
	{
		return EQuizAnswerResult::BadEntry;
	}

	return PlayerController->AnswerQuestion(CurrentQuestionId, Input);
}

void UHeavenCartridge::TriggerAfterLostGame()
{
	UE_LOG(LogPlayer, Display, TEXT("%s: TriggerAfterLostGame"), *GetName());

	auto PlayerController = GetPlayerController();
	if (PlayerController)
	{
		PlayerController->GameHasEnded(GetOwner(), false);
	}
}

AHeavenPlayerController* UHeavenCartridge::GetPlayerController() const
{
	auto PlayerController = Cast<AHeavenPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (!PlayerController)
	{
		UE_LOG(LogPlayer, Error, TEXT("%s: No player controller!"), *GetName());
	} 

	return PlayerController;
}
