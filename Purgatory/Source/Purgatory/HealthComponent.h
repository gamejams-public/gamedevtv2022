// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PURGATORY_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UHealthComponent();

	UFUNCTION(BlueprintPure)
	bool IsDead() const;

	UFUNCTION(BlueprintPure)
	float GetHealthPercent() const;

	UFUNCTION(BlueprintPure)
	float GetHealth() const;

	UFUNCTION(BlueprintPure)
	float GetMaxHealth() const;

	void SetHealth(float Health);
	void SetMaxHealth(float MaxHealth);

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY(EditDefaultsOnly)
	float MaxHealth{ 100.0f };

	UPROPERTY(VisibleAnywhere)
	float Health{};
};

/////// INLINE FUNCTIONS /////////

inline void UHealthComponent::SetHealth(float TheHealth)
{
	this->Health = TheHealth;
}

inline void UHealthComponent::SetMaxHealth(float TheMaxHealth)
{
	this->MaxHealth = TheMaxHealth;
}

inline float UHealthComponent::GetHealth() const
{
	return Health;
}

inline float UHealthComponent::GetMaxHealth() const
{
	return MaxHealth;
}

inline float UHealthComponent::GetHealthPercent() const
{
	return Health / MaxHealth;
}

inline bool UHealthComponent::IsDead() const
{
	return Health <= 0;
}
