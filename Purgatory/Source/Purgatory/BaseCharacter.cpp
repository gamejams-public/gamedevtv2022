// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"

#include "TP_WeaponComponent.h"
#include "Weapon.h"
#include "HealthComponent.h"
#include "Components/CapsuleComponent.h"

#include "Logging.h"
#include "Noise.h"

#include "BasePurgatoryGameMode.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABaseCharacter::ABaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));
}

void ABaseCharacter::FireWeapon()
{
	if (!HasWeapon())
	{
		return;
	}

	// Trigger the OnItemUsed Event
	OnUseItem.Broadcast();

	const auto& NoiseConfig = Noise::GunFire;

	MakeNoise(Noise::GunFire,GetActorLocation());
}

void ABaseCharacter::EquipStartingWeapon()
{
	if (!HasWeapon())
	{
		SpawnAttachedStartingWeapon();
	}
}

void ABaseCharacter::AttachWeapon(UTP_WeaponComponent* WeaponComponent)
{
	if (!WeaponComponent)
	{
		return;
	}

	bHasWeapon = true;

	// Register so that Fire is called every time the character tries to use the item being held
	OnUseItem.AddDynamic(WeaponComponent, &UTP_WeaponComponent::Fire);
}

void ABaseCharacter::DetachWeapon(UTP_WeaponComponent* WeaponComponent)
{
	if (!WeaponComponent)
	{
		return;
	}

	bHasWeapon = false;

	// Unregister from the OnUseItem Event
	OnUseItem.RemoveDynamic(WeaponComponent, &UTP_WeaponComponent::Fire);
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ABaseCharacter::SpawnAttachedStartingWeapon()
{
	UWorld* World = GetWorld();

	if (!World)
	{
		UE_LOG(LogWeapon, Error, TEXT("%s: Unable to spawn AWeapon actor class %s as World is NULL"),
			*GetName(),
			StartingWeaponClass ? *StartingWeaponClass->GetName() : TEXT("NULL")
		);
		return;
	}

	auto Weapon = World->SpawnActor<AWeapon>(StartingWeaponClass);

	if (!Weapon)
	{
		UE_LOG(LogWeapon, Warning, TEXT("%s: Unable to spawn AWeapon actor class %s"),
			*GetName(),
			StartingWeaponClass ? *StartingWeaponClass->GetName() : TEXT("NULL")
		);
		return;
	}

	Weapon->AttachTo(this);
}

void ABaseCharacter::FellOutOfWorld(const class UDamageType& dmgType)
{
	UE_LOG(LogPawn, Display, TEXT("%s: Fell out of world!"), *GetName());

	Killed();
}

void ABaseCharacter::MakeNoise(const Noise::FNoiseConfig& NoiseConfig, const FVector& Location)
{
	UE_LOG(LogPawn, Verbose, TEXT("%s: Made noise %s at location=%s"), *GetName(), *NoiseConfig.Tag.ToString(), *Location.ToCompactString());

	Super::MakeNoise(NoiseConfig.Loudness, this, Location, NoiseConfig.MaxRange, NoiseConfig.Tag);
}

void ABaseCharacter::Killed()
{
	UE_LOG(LogPawn, Display, TEXT("Actor %s is DEAD"), *GetName());

	bDead = true;

	auto GameMode = GetWorld()->GetAuthGameMode<ABasePurgatoryGameMode>();
	if (GameMode)
	{
		GameMode->PawnKilled(this);
	}

	// Avoid being active after actor is dead
	DetachFromControllerPendingDestroy();

	// Remove the capsule component to prevent collisions after character is dead
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ABaseCharacter::OnInit(float Health, float MaxHealth)
{
}

void ABaseCharacter::OnAnyDamage(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator)
{
}

void ABaseCharacter::OnGainedHealth(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator)
{
}

void ABaseCharacter::OnHurt(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator)
{
}

void ABaseCharacter::OnKilled(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator)
{
	Killed();
}

void ABaseCharacter::PlaySoundAtActorLocation(USoundBase* Sound)
{
	if (!Sound)
	{
		return;
	}

	UGameplayStatics::PlaySoundAtLocation(this, Sound, GetActorLocation());
}
