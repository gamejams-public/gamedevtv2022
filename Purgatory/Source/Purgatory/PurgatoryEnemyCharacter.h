// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseCharacter.h"

#include "PurgatoryEnemyCharacter.generated.h"

class UCapsuleComponent;

UCLASS()
class PURGATORY_API APurgatoryEnemyCharacter : public ABaseCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APurgatoryEnemyCharacter();

	virtual void PlayFireWeaponAnimation(UAnimMontage* FireWeaponAnimationMontage) override;
	virtual void AttachWeapon(UTP_WeaponComponent* WeaponComponent) override;

	UFUNCTION(BlueprintCallable)
	void SetBadGuyProbability(float Probability, float TurnMinTime, float TurnMaxTime);

	UFUNCTION(BlueprintPure)
	bool IsBadGuy() const;

	UFUNCTION(BlueprintPure)
	bool IsOrWillBecomeABadGuy() const;

	virtual bool ShouldTakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void OnHurt(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator) override;
	virtual void OnKilled(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator) override;

private:
	UFUNCTION()
	void BadGuyDecision();

	UFUNCTION()
	void InitAfterBeginPlay();

private:
	UPROPERTY(Category = "Movement", VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	bool JumpButtonDown{};

	UPROPERTY(Category = "Movement", VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	bool CrouchButtonDown{};

	UPROPERTY(Category = "Collision", VisibleAnywhere)
	class UCapsuleComponent* AgroOverlapVolume{};

	// TODO: These will be changed during spawning
	UPROPERTY(Category = "Enemy", EditAnywhere)
	float BadGuyProbability{ 0.4f };

	UPROPERTY(Category = "Enemy", EditAnywhere)
	float BadGuyTurnMinTime{ 5.0f };

	UPROPERTY(Category = "Sounds", EditAnywhere)
	USoundBase* EnemyHurtSound {};

	UPROPERTY(Category = "Sounds", EditAnywhere)
	USoundBase* EnemyDeathSound {};

	UPROPERTY(Category = "Enemy", EditAnywhere)
	float BadGuyTurnMaxTime{ 30.0f };

	bool bBadGuy{};
	float BadGuyTurnTimeSeconds{};
	bool bWillBecomeBadGuy{};

	FTimerHandle BadGuyFireTimerHandle{};
	FTimerHandle InitTimerHandle{};
};

inline void APurgatoryEnemyCharacter::SetBadGuyProbability(float Probability, float TurnMinTime, float TurnMaxTime)
{
	BadGuyProbability = Probability;
	BadGuyTurnMinTime = TurnMinTime;
	BadGuyTurnMaxTime = TurnMaxTime;
}

inline bool APurgatoryEnemyCharacter::IsBadGuy() const
{
	return bBadGuy;
}

inline bool APurgatoryEnemyCharacter::IsOrWillBecomeABadGuy() const
{
	return bWillBecomeBadGuy;
}
