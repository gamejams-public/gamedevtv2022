// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"

#include "TP_WeaponComponent.h"

// Sets default values
AWeapon::AWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	WeaponComponent = CreateDefaultSubobject<UTP_WeaponComponent>(TEXT("TP_Weapon"));
}

void AWeapon::AttachTo(ABaseCharacter* TargetCharacter)
{
	if (!WeaponComponent)
	{
		return;
	}

	WeaponComponent->AttachWeapon(TargetCharacter);
}

void AWeapon::Fire()
{
	if (!WeaponComponent)
	{
		return;
	}

	WeaponComponent->Fire();
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();
}

