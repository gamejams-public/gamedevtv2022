// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Console/Cartridge.h"
#include "Quiz.h"

#include "HeavenCartridge.generated.h"

class AHeavenPlayerController;

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PURGATORY_API UHeavenCartridge : public UCartridge
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	virtual void OnInput(const FString& Input) override;

private:

	void PresentQuestion();
	EQuizAnswerResult ProcessAnswer(const FString& Input);

	AHeavenPlayerController* GetPlayerController() const;

	UFUNCTION()
	void TriggerAfterLostGame();

private:
	bool bStarted{};
	bool bGameOver{};
	int32 CurrentQuestionId{};

	UPROPERTY(Category = "Timers", EditDefaultsOnly)
	float LoseTimeTravelDelay{ 4.0f };

	FTimerHandle LostTravelDelayTimer{};
};
