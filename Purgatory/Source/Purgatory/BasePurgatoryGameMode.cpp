// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePurgatoryGameMode.h"

#include "Logging.h"
#include "EngineUtils.h"

#include "PurgatoryGameStateBase.h"

namespace
{
	constexpr float APurgatoryGameModeInitDelay = 0.2f;
}

void ABasePurgatoryGameMode::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogGamePurgatory, Display, TEXT("%s: BeginPlay"), *GetName());

	auto State = GetGameState<APurgatoryGameStateBase>();
	verify(State);

	GetWorldTimerManager().SetTimer(InitTimerHandle, State, &APurgatoryGameStateBase::Init, APurgatoryGameModeInitDelay);
}

void ABasePurgatoryGameMode::PawnKilled(APawn* PawnKilled)
{
	UE_LOG(LogGamePurgatory, Log, TEXT("%s: Pawn %s was killed"), *GetName(), (PawnKilled ? *PawnKilled->GetName() : TEXT("NULL")));

	auto PurgatoryGameState = GetGameState<APurgatoryGameStateBase>();
	
	if (!PurgatoryGameState)
	{
		UE_LOG(LogGameMode, Error, TEXT("%s : PawnKilled - GameState is not APurgatoryGameStateBase"), *GetName());
		return;
	}

	PurgatoryGameState->PawnKilled(PawnKilled);
}

void ABasePurgatoryGameMode::EndGame(bool bIsPlayerWinner)
{
	UE_LOG(LogGameMode, Display, TEXT("%s: EndGame: bIsPlayerWinner=%s"), *GetName(), bIsPlayerWinner ? TEXT("TRUE") : TEXT("FALSE"));

	for (AController* Controller : TActorRange<AController>(GetWorld()))
	{
		const bool bIsPlayer = Controller->IsPlayerController();
		const bool bPawnWon = bIsPlayerWinner == bIsPlayer;

		UE_LOG(LogGameMode, Log, TEXT("Controller %s bIsPlayerWinner=%s;won=%s;bIsPlayer=%s"),
			(*Controller->GetName()),
			(bIsPlayerWinner ? TEXT("TRUE") : TEXT("FALSE")),
			(bPawnWon ? TEXT("TRUE") : TEXT("FALSE")),
			(bIsPlayer ? TEXT("TRUE") : TEXT("FALSE"))
		);

		Controller->GameHasEnded(Controller->GetPawn(), bPawnWon);
	}
}

APurgatoryGameStateBase* ABasePurgatoryGameMode::GetPurgatoryGameState(const char* LoggingFunctionName)
{
	auto State = GetGameState<APurgatoryGameStateBase>();

	if (!State)
	{
		auto ActualState = GetGameState<AGameStateBase>();
		UE_LOG(LogGameMode, Error, TEXT("%s:%s - GameState %s is not APurgatoryGameStateBase"),
			*GetName(),
			UTF8_TO_TCHAR(LoggingFunctionName),
			ActualState ? *ActualState->StaticClass()->GetName() : TEXT("NULL"));
	}

	return State;
}
