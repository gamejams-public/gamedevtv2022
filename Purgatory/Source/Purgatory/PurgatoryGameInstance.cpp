// Fill out your copyright notice in the Description page of Project Settings.


#include "PurgatoryGameInstance.h"

#include "Misc/ConfigCacheIni.h"


#include "Kismet/GameplayStatics.h"

#include "PurgatoryGameStateBase.h"

#include "Logging.h"

void UPurgatoryGameInstance::Init()
{
    Super::Init();

    UE_LOG(LogGamePurgatory, Display, TEXT("%s: Init"), *GetName());
}

FString UPurgatoryGameInstance::GetProjectVersion() const
{
    FString GameVersion;
    GConfig->GetString(
        TEXT("/Script/EngineSettings.GeneralProjectSettings"),
        TEXT("ProjectVersion"),
        GameVersion,
        GGameIni
    );

    return FString::Printf(TEXT("v %s"), *GameVersion);
}

void UPurgatoryGameInstance::CompletedMap(bool bWon)
{
    UE_LOG(LogGamePurgatory, Log, TEXT("%s: CompletedMap: bWon=%s"), *GetName(), (bWon ? TEXT("TRUE") : TEXT("FALSE")));

    UWorld* World = GetWorld();
    if (!World)
    {
        UE_LOG(LogGamePurgatory, Error, TEXT("%s: CompletedMap - GetWorld() was NULL"), *GetName());
        return;
    }

    auto GameState = Cast<APurgatoryGameStateBase>(World->GetGameState());
    if (!GameState)
    {
        UE_LOG(LogGamePurgatory, Error, TEXT("%s: CompletedMap GameState was not APurgatoryGameStateBase"), *GetName(),
            World->GetGameState() ? *World->GetGameState()->GetName() : TEXT("NULL"));
        return;
    }

    auto Stage = GameState->GetGameStage();

    switch (Stage)
    {
        case EGameStage::Purgatory:
        {
            if (bWon)
            {
                LoadHeavenMap();
            }
            else
            {
                LoadHellMap();
            }
            break;
        }
        case EGameStage::Hell:
        {
            if (bWon)
            {
                LoadHeavenMap();
            }
            else
            {
                RestartMap();
            }
            break;
        }
        case EGameStage::Heaven:
        {
            if (bWon)
            {
                // TODO: 
            }
            else
            {
                LoadHellMap();
 }
            break;
        }
    }
}

void UPurgatoryGameInstance::QuitGame()
{
    auto PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
    if (!PlayerController)
    {
        UE_LOG(LogGamePurgatory, Error, TEXT("%s: PlayerController was NULL"), *GetName());
        return;
    }

    PlayerController->ConsoleCommand("quit");
}

void UPurgatoryGameInstance::RestartGame()
{
    LoadPurgatoryMap();
}

void UPurgatoryGameInstance::LoadPurgatoryMap()
{
    LoadMap(PurgatoryMapName);
}

void UPurgatoryGameInstance::LoadHellMap()
{
    LoadMap(HellMapName);
}

void UPurgatoryGameInstance::LoadHeavenMap()
{
    LoadMap(HeavenMap);
}

void UPurgatoryGameInstance::LoadMap(const FString& Name)
{
    UWorld* World = GetWorld();
    if (!World)
    {
        UE_LOG(LogGamePurgatory, Error, TEXT("%s: LoadMap %s GetWorld() was NULL"), *GetName(), *Name);
        return;
    }

    //FString MapPath("/Game/Maps/");
    //MapPath.Append(Name);

    UGameplayStatics::OpenLevel(World, FName(Name));
}

void UPurgatoryGameInstance::RestartMap()
{
    auto PlayerController = GetFirstLocalPlayerController();
    if (!PlayerController)
    {
        UE_LOG(LogGamePurgatory, Error, TEXT("%s: RestartMap - Player Controller was NULL"), *GetName());
        return;
    }

    PlayerController->RestartLevel();
}
