// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePurgatoryGameMode.h"
#include "Quiz.h"

#include <vector>

#include "HeavenGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PURGATORY_API AHeavenGameMode : public ABasePurgatoryGameMode, public IQuiz
{
	GENERATED_BODY()
	
public:

	virtual void BeginPlay() override;

	// Inherited via IQuiz
	virtual const FQuizQuestion* GetNextQuestion() override;
	virtual EQuizAnswerResult AnswerQuestion(int32 QuestionId, const FString& AnswerValue) override;

private:

	std::vector<FQuizQuestion> QuizQuestions;

	UPROPERTY(Category = "Quiz", EditDefaultsOnly)
	int32 NumQuestions { 3 };
};
