// Fill out your copyright notice in the Description page of Project Settings.


#include "PurgatoryGameStateBase.h"

#include "PurgatoryEnemyCharacter.h"

#include "Logging.h"

void APurgatoryGameStateBase::Init()
{
	ActiveCharacters.Empty();

	for (TObjectIterator<APurgatoryEnemyCharacter> Itr; Itr; ++Itr)
	{
		// Take first game world - only one instance should exist
		UWorld* InstanceWorld = Itr->GetWorld();

		//World Check to avoid getting objects from the editor world when doing PIE
		if (Itr->GetWorld() != GetWorld())
		{
			continue;
		}

		ActiveCharacters.Add(*Itr);
	}

	UE_LOG(LogGameMode, Display, TEXT("%s: Enemies=%d; Friendlies=%d"),
		*GetName(),
		GetPotentialEnemyAICount(),
		GetFriendlyAICount()
	);
}

void APurgatoryGameStateBase::PawnKilled(APawn* PawnKilled)
{
	auto EnemyPawn = Cast<APurgatoryEnemyCharacter>(PawnKilled);
	if (!EnemyPawn)
	{
		return;
	}

	ActiveCharacters.Remove(EnemyPawn);
}

void APurgatoryGameStateBase::PawnAdded(APawn* Pawn)
{
	auto EnemyPawn = Cast<APurgatoryEnemyCharacter>(Pawn);
	if (!EnemyPawn)
	{
		return;
	}

	ActiveCharacters.Add(EnemyPawn);
}

int32 APurgatoryGameStateBase::GetFriendlyAICount() const
{
	int32 Count = 0;
	for (const auto& Enemy : ActiveCharacters)
	{
		if (!Enemy->IsOrWillBecomeABadGuy())
		{
			++Count;
		}
	}

	return Count;
}

int32 APurgatoryGameStateBase::GetEnemyAICount() const
{
	int32 Count = 0;
	for (const auto& Enemy : ActiveCharacters)
	{
		if (Enemy->IsBadGuy())
		{
			++Count;
		}
	}

	return Count;
}

int32 APurgatoryGameStateBase::GetPotentialEnemyAICount() const
{
	int32 Count = 0;
	for (const auto& Enemy : ActiveCharacters)
	{
		if (Enemy->IsOrWillBecomeABadGuy())
		{
			++Count;
		}
	}

	return Count;
}
