// Copyright Epic Games, Inc. All Rights Reserved.

#include "PurgatoryProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"

#include "Logging.h"

// TODO: Switch to use IDamageable so that this works with non-character based enemies like APawn derived classes like a "Spider"
#include "BaseCharacter.h"

APurgatoryProjectile::APurgatoryProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(CollisionComp);

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;
}

void APurgatoryProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (auto Character = Cast<ABaseCharacter>(OtherActor); Character)
	{
		UE_LOG(LogWeapon, Log, TEXT("%s: Applying %f damage to %s"), *GetName(), DamageAmount, *Character->GetName());

		FPointDamageEvent DamageEvent;
		// TODO: Handle location based damage for enemies
		Character->TakeDamage(DamageAmount, DamageEvent, this->GetInstigatorController(), GetInstigator());

		Destroy();
	}
	else
	{
		// Hit some other object, blunt the damage
		UE_LOG(LogWeapon, Verbose, TEXT("%s: Hit %s-%s reducing damage from %f to %f"),
			*GetName(),
			OtherActor ? *OtherActor->GetName() : TEXT("NULL"),
			OtherComp ? *OtherComp->GetName() : TEXT("NULL"),
			DamageAmount,
			DamageAmount - BounceDamageReduction);

		DamageAmount -= BounceDamageReduction;

		if (DamageAmount < 0)
		{
			Destroy();
		}

	}
}

void APurgatoryProjectile::BeginPlay()
{
	Super::BeginPlay();

	DamageAmount = BaseDamage;

	CollisionComp->OnComponentHit.AddDynamic(this, &APurgatoryProjectile::OnHit);
}
