// Fill out your copyright notice in the Description page of Project Settings.


#include "PurgatoryAIController.h"

#include "Logging.h"
#include "ArrayUtils.h"
#include "PurgatoryEnemyBlackboardKeys.h"
#include "PurgatoryCharacter.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h" 
#include "Perception/AIPerceptionComponent.h"

#include "Perception/AIPerceptionSystem.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AISense_Hearing.h"


APurgatoryAIController::APurgatoryAIController()
{
	//PrimaryActorTick.bCanEverTick = true;

	// PerceptionComponent is defined as a public field and is NULL by default in AAIController base class
	PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception"));
}

//void APurgatoryAIController::Tick(float DeltaTime)
//{
//	Super::Tick(DeltaTime);
//
//
//}



void APurgatoryAIController::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogAI, Log, TEXT("%s: BeginPlay"), *GetName());

	APawn* APawn = GetPawn();
	if (ShouldInitWithPawn(APawn))
	{
		InitWithPawn(Cast<APurgatoryEnemyCharacter>(APawn));
	}
}

void APurgatoryAIController::OnPossess(APawn* InPawn)
{
	UE_LOG(LogAI, Log, TEXT("%s: OnPossess: %s"), *GetName(), (InPawn ? *InPawn->GetName() : TEXT("NULL")));

	const bool bShouldInitWithPawn = ShouldInitWithPawn(InPawn);

	Super::OnPossess(InPawn);

	if (bShouldInitWithPawn)
	{
		InitWithPawn(Cast<APurgatoryEnemyCharacter>(InPawn));
	}
}

// Note that with spawned actors order is different and this gets called before receiving a pawn
// https://answers.unrealengine.com/questions/1004334/spawn-actor-and-aicontroller-problem.html
/*
	 - Spawn:

	LogBlueprintUserMessages: [BP_AI_C_2] Controller Begin Play

	LogBlueprintUserMessages: [BP_Pawn_C_1] Pawn Possessed

	LogBlueprintUserMessages: [BP_AI_C_2] Controller Possessed

	LogBlueprintUserMessages: [BP_Pawn_C_1] Pawn Begin Play

	- Level Actor:

	LogBlueprintUserMessages: [BP_Pawn_2] Pawn Possessed

	LogBlueprintUserMessages: [BP_AI_C_0] Controller Possessed

	LogBlueprintUserMessages: [BP_AI_C_0] Controller Begin Play

	LogBlueprintUserMessages: [BP_Pawn_2] Pawn Begin Play
*/
void APurgatoryAIController::InitWithPawn(APurgatoryEnemyCharacter* InPawn)
{
	UE_LOG(LogAI, Log, TEXT("%s: InitWithPawn: %s"), *GetName(), (InPawn ? *InPawn->GetName() : TEXT("NULL")));

	this->AIPawn = InPawn;

	if (!AIBehavior)
	{
		UE_LOG(LogAI, Error, TEXT("No AIBehavior tree associated with AIController %s"), *GetName());
		return;
	}

	InitPerception();
	RunBehaviorTree(AIBehavior);

	if (!Blackboard)
	{
		UE_LOG(LogAI, Warning, TEXT("%s: No blackboard defined!"), *GetName());
		return;
	}

	// TODO: Initialize any blackboard Keys

	UE_LOG(LogAI, Log, TEXT("%s-%s: Initialized with BehaviorTree=%s and Blackboard=%s"), *GetName(), *GetPossessedPawnName(), *AIBehavior->GetName(), *Blackboard->GetName());
}


bool APurgatoryAIController::ShouldInitWithPawn(APawn* InPawn) const
{
	auto EnemyPawn = Cast<APurgatoryEnemyCharacter>(InPawn);

	return HasActorBegunPlay() && EnemyPawn && EnemyPawn != AIPawn;
}

void APurgatoryAIController::InitPerception()
{
	PerceptionComponent->OnPerceptionUpdated.AddDynamic(this, &APurgatoryAIController::OnPerceptionUpdated);
}

void APurgatoryAIController::OnPerceptionUpdated(const TArray<AActor*>& UpdatedActors)
{
	UE_LOG(LogAI, Log, TEXT("%s-%s: OnPerceptionUpdated: UpdatedCount=%d;UpdatedActors=%s"), *GetName(), *GetPossessedPawnName(),
		UpdatedActors.Num(), *PurgatoryGame::ToStringObjectElements(UpdatedActors));

	CurrentlySeenActors.Empty();
	CurrentlyHeardActors.Empty();

	PerceptionComponent->GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), CurrentlySeenActors);
	PerceptionComponent->GetCurrentlyPerceivedActors(UAISense_Hearing::StaticClass(), CurrentlyHeardActors);

	UE_LOG(LogAI, Log, TEXT("%s-%s: OnPerceptionUpdated: SeenCount=%d;SeenActors=%s"), *GetName(), *GetPossessedPawnName(),
		CurrentlySeenActors.Num(), *PurgatoryGame::ToStringObjectElements(CurrentlySeenActors));

	UE_LOG(LogAI, Log, TEXT("%s-%s: OnPerceptionUpdated: HeardCount=%d;HeardActors=%s"), *GetName(), *GetPossessedPawnName(),
		CurrentlyHeardActors.Num(), *PurgatoryGame::ToStringObjectElements(CurrentlyHeardActors));

	if (!Blackboard)
	{
		UE_LOG(LogAI, Warning, TEXT("%s: No blackboard defined!"), *GetName());
		return;
	}

	auto* SeenPlayer = GetFirstMatchingActorByClass<APurgatoryCharacter>(CurrentlySeenActors);
	auto* HeardPlayer = GetFirstMatchingActorByClass<APurgatoryCharacter>(CurrentlyHeardActors);

	// Shouldn't hear and see different players since only one player character but may see but not hear the player and vice versa
	check(!SeenPlayer || !HeardPlayer || SeenPlayer == HeardPlayer);

	FActorPerceptionBlueprintInfo ActorPerception;
	PerceptionComponent->GetActorsPerception(SeenPlayer ? SeenPlayer : HeardPlayer, ActorPerception);

	if (auto HearingStimulus = GetMatchingActiveStimulusByClass<UAISense_Hearing>(ActorPerception);
		HearingStimulus&& HeardPlayer)
	{
		UE_LOG(LogAI, Log, TEXT("%s-%s: Heard player - setting noise detection key and location=%s"),
			*GetName(), *GetPossessedPawnName(), *HearingStimulus->StimulusLocation.ToCompactString());

		Blackboard->SetValueAsBool(PurgatoryEnemyBlackboardKeys::NoiseDetectedKey, true);
		Blackboard->SetValueAsVector(PurgatoryEnemyBlackboardKeys::NoiseLastLocationKey, HearingStimulus->StimulusLocation);
	}
	else
	{
		UE_LOG(LogAI, Log, TEXT("%s-%s: Heard player - noise detection timeout out, setting key to false"),
			*GetName(), *GetPossessedPawnName());

		Blackboard->SetValueAsBool(PurgatoryEnemyBlackboardKeys::NoiseDetectedKey, false);
		//Blackboard->ClearValue(PurgatoryEnemyBlackboardKeys::NoiseLastLocationKey);
	}

	if (auto SightStimulus = GetMatchingActiveStimulusByClass<UAISense_Sight>(ActorPerception);
		SightStimulus && SeenPlayer)
	{
		UE_LOG(LogAI, Log, TEXT("%s-%s: Saw player - setting seen detection key and location=%s"),
			*GetName(), *GetPossessedPawnName(), *SightStimulus->StimulusLocation.ToCompactString());

		Blackboard->SetValueAsObject(PurgatoryEnemyBlackboardKeys::PlayerSeenKey, SeenPlayer);
		Blackboard->SetValueAsVector(PurgatoryEnemyBlackboardKeys::PlayerLastSeenLocationKey, SightStimulus->StimulusLocation);
	}
	else
	{
		UE_LOG(LogAI, Log, TEXT("%s-%s: Saw player - seen detection timed out, setting key to false"),
			*GetName(), *GetPossessedPawnName());

		Blackboard->ClearValue(PurgatoryEnemyBlackboardKeys::PlayerSeenKey);
		//Blackboard->ClearValue(PurgatoryEnemyBlackboardKeys::PlayerLastSeenLocationKey);
	}
}

void APurgatoryAIController::DamagedByActor(AActor* Actor)
{
	if (!Actor)
	{
		return;
	}

	UE_LOG(LogAI, Log, TEXT("%s-%s: Hit by player - setting %s to %s"),
		*GetName(), *GetPossessedPawnName(),
		*PurgatoryEnemyBlackboardKeys::HitFromLocationKey.ToString(),
		*Actor->GetActorLocation().ToCompactString()
	);

	Blackboard->SetValueAsVector(PurgatoryEnemyBlackboardKeys::HitFromLocationKey, Actor->GetActorLocation());
}

void APurgatoryAIController::InstigatedAnyDamage(float Damage, const class UDamageType* DamageType, class AActor* DamagedActor, class AActor* DamageCauser)
{
	Super::InstigatedAnyDamage(Damage, DamageType, DamagedActor, DamageCauser);
}