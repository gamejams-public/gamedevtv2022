// Fill out your copyright notice in the Description page of Project Settings.


#include "RighteousMeterComponent.h"

#include "PurgatoryEnemyCharacter.h"
#include "Logging.h"

// Sets default values for this component's properties
URighteousMeterComponent::URighteousMeterComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}


void URighteousMeterComponent::HurtBy(AActor* Instigator)
{
	if (!Instigator)
	{
		return;
	}

	const auto SeenCount = RecordConsecutiveHurtByCount(Instigator);
	if (SeenCount >= ConsecutiveDamageCount && !ConsecutiveHitCountPenaltyActors.Contains(Instigator))
	{
		UE_LOG(LogPlayer, Log, TEXT("Taking consecutive damage from %s %d times - Applying poor performance penalty"),
			*Instigator->GetName(), SeenCount);
		UpdateRighteousMeter(-ConsecutiveDamagePenalty);

		ConsecutiveHitCountPenaltyActors.Add(Instigator);
	}
}

void URighteousMeterComponent::Hurt(AActor* Instigator)
{
	auto Enemy = Cast<APurgatoryEnemyCharacter>(Instigator);

	if (!Enemy)
	{
		return;
	}

	// reset the consecutive hit count count
	ResetConsecutiveHurtByCount(Instigator);

	// Already noted if we shot first at an unarmed opponent
	// make sure we do not penalize again or reward if previously penalized
	if (HurtActors.Contains(Instigator))
	{
		return;
	}

	bool bUpdated{};

	if (Enemy->IsBadGuy() && Enemy->IsDead())
	{
		bUpdated = true;

		UE_LOG(LogPlayer, Log, TEXT("Killed bad guy %s - Adding performance reward"), *Enemy->GetName());
		UpdateRighteousMeter(RewardAmount);
	}
	else if (!Enemy->IsBadGuy())
	{
		bUpdated = true;

		UE_LOG(LogPlayer, Log, TEXT("Hurt good guy %s - Adding performance penalty"), *Enemy->GetName());
		UpdateRighteousMeter(-MistakePenaltyAmount);
	}

	if (bUpdated)
	{
		HurtActors.Add(Instigator);
	}
}

void URighteousMeterComponent::Suicide()
{
	UE_LOG(LogPlayer, Log, TEXT("Suicide - punished with a 0"));

	UpdateRighteousMeter(-RighteousPercent);
}

// Called when the game starts
void URighteousMeterComponent::BeginPlay()
{
	Super::BeginPlay();
}

int32 URighteousMeterComponent::RecordConsecutiveHurtByCount(AActor* Actor)
{
	return ++AggressorActors.FindOrAdd(Actor, 0);
}

void URighteousMeterComponent::ResetConsecutiveHurtByCount(AActor* Actor)
{
	int32* ConsecutiveHitCount = AggressorActors.Find(Actor);
	if (ConsecutiveHitCount)
	{
		*ConsecutiveHitCount = 0;
	}
}

void URighteousMeterComponent::UpdateRighteousMeter(float DeltaAmount)
{
	float PreviousValue = RighteousPercent;
	RighteousPercent = FMath::Clamp(RighteousPercent + DeltaAmount, 0.0f, 1.0f);

	UE_LOG(LogPlayer, Log, TEXT("UpdateRighteousMeter %f -> %f"), PreviousValue, RighteousPercent);
}


