#pragma once

#include "CoreMinimal.h"

namespace PurgatoryEnemyBlackboardKeys
{
	inline const FName NoiseDetectedKey = "NoiseDetected";
	inline const FName NoiseLastLocationKey = "NoiseLastLocation";

	inline const FName PlayerSeenKey = "PlayerSeen";
	inline const FName PlayerLastSeenLocationKey = "PlayerSeenLocation";
	inline const FName HitFromLocationKey = "HitFromLocation";
}