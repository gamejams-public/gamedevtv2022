// Fill out your copyright notice in the Description page of Project Settings.


#include "PurgatoryPlayerController.h"

#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h" 
#include "GameFramework/SpectatorPawn.h"

#include "BaseCharacter.h"
#include "PurgatoryCharacter.h"
#include "RighteousMeterComponent.h"

#include "Logging.h"
#include "PurgatoryGameInstance.h"

#include "KeyBindings.h"

namespace
{
	constexpr float GameEndedOffsetDelay = 0.2f;
}

void APurgatoryPlayerController::InstigatedAnyDamage(float Damage, const UDamageType* DamageType, AActor* DamagedActor, AActor* DamageCauser)
{
	UE_LOG(LogPlayer, Verbose, TEXT("%s - APurgatoryPlayerController::InstigatedAnyDamage - Damage=%f; DamagedActor=%s; DamageCauser=%s"),
		*GetName(), Damage, DamagedActor ? *DamagedActor->GetName() : TEXT("NULL"), DamageCauser ? *DamageCauser->GetName() : TEXT("NULL"));

	Super::InstigatedAnyDamage(Damage, DamageType, DamagedActor, DamageCauser);

	// TODO: Here is where we can get credit for a kill or damage points

	auto PurgatoryPlayerCharacter = Cast<APurgatoryCharacter>(GetPawn());
	auto Enemy = Cast<ABaseCharacter>(DamagedActor);

	if (!Enemy || !PurgatoryPlayerCharacter)
	{
		return;
	}

	auto RighteousMeterComponent = PurgatoryPlayerCharacter->GetRighteousMeterComponent();

	if (!RighteousMeterComponent)
	{
		return;
	}

	RighteousMeterComponent->Hurt(Enemy);
}

void APurgatoryPlayerController::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogPlayer, Display, TEXT("%s: BeginPlay"), *GetName());

	ShowHUD();
}

void APurgatoryPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	PlayerPawn = InPawn;

	UE_LOG(LogPlayer, Display, TEXT("%s: Has possessed pawn %s"), *GetName(), InPawn ? *InPawn->GetName() : TEXT("NULL"));
}

void APurgatoryPlayerController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner)
{
	Super::GameHasEnded(EndGameFocus, bIsWinner);

	UE_LOG(LogPlayer, Display,
		TEXT("%s: GameHasEnded - EndGameFocus=%s;bIsWinner=%s"),
		*GetName(),
		(EndGameFocus ? *EndGameFocus->GetName() : TEXT("NULL")),
		(bIsWinner ? TEXT("TRUE") : TEXT("FALSE"))
	);

	FTimerDelegate GameEndDelegate = FTimerDelegate::CreateLambda([this, bIsWinner]()
		{
			this->TriggerEndOfLevel(bIsWinner);
		});

	GetWorldTimerManager().SetTimer(GameEndedDelayTimer, GameEndDelegate, GameEndedOffsetDelay, false);
}

void APurgatoryPlayerController::HideHUD()
{
	RemoveCurrentWidget();
}

void APurgatoryPlayerController::ShowHUD()
{
	DisplayWidgetClass(HUDClass);
}

void APurgatoryPlayerController::SetInputModeUI()
{
	if (!CurrentWidget)
	{
		UE_LOG(LogUI, Warning, TEXT("%s: Attempted to SetInputModeUI when there is no widget displayed!"), *GetName());
		return;
	}

	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(CurrentWidget->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	SetInputMode(InputModeData);
	bShowMouseCursor = true;
}

void APurgatoryPlayerController::SetInputModeGame()
{
	RemoveCurrentWidget();

	FInputModeGameOnly InputModeData;
	SetInputMode(InputModeData);

	bShowMouseCursor = false;
}

void APurgatoryPlayerController::PauseGame()
{
	if (!PauseMenuClass)
	{
		UE_LOG(LogUI, Warning, TEXT("%s: No pause menu configured"), *GetName());
		return;
	}

	DisplayWidgetClass(PauseMenuClass);

	// TODO: Toggle blur on player character or world actor

	UGameplayStatics::SetGamePaused(GetWorld(), true);

	SetInputModeUI();
}

void APurgatoryPlayerController::ResumeGame()
{
	// TODO: Toggle blur off player character or world actor

	UGameplayStatics::SetGamePaused(GetWorld(), false);

	SetInputModeGame();

	ShowHUD();
}

void APurgatoryPlayerController::DisplayWidgetClass(TSubclassOf<class UUserWidget> WidgetClass)
{
	if (!IsLocalPlayerController())
	{
		// Don't try to display widgets on the server
		return;
	}

	RemoveCurrentWidget();

	if (!WidgetClass)
	{
		UE_LOG(LogUI, Error, TEXT("%s: No Widget class set"), *GetName());
		return;
	}

	CurrentWidget = CreateWidget(this, WidgetClass);
	if (CurrentWidget)
	{
		// Need to make visible 
		CurrentWidget->AddToViewport();
		UE_LOG(LogUI, Log, TEXT("%s: Displayed widget %s in viewport"), *GetName(), *CurrentWidget->GetName());
	}
	else
	{
		UE_LOG(LogUI, Error, TEXT("%s: Widget could need be found: %s"), *GetName(), WidgetClass ? *WidgetClass->GetName() : TEXT("NULL"));
	}
}

void APurgatoryPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction(KeyBindings::Pause, EInputEvent::IE_Pressed, this, &APurgatoryPlayerController::PauseGame);
}

void APurgatoryPlayerController::OnSpectatorPawnActivated(APawn* MySpectatorPawn, APawn* PreviousPlayerPawn)
{
}

void APurgatoryPlayerController::RemoveCurrentWidget()
{
	if (!CurrentWidget)
	{
		return;
	}

	UE_LOG(LogUI, Display, TEXT("Removing CurrentWidget=%s"), *CurrentWidget->GetName());
	CurrentWidget->RemoveFromViewport();

	CurrentWidget = nullptr;
}

void APurgatoryPlayerController::TriggerEndOfLevel(bool bIsWinner)
{
	Spectate();

	auto PurgatoryGameInstance = Cast<UPurgatoryGameInstance>(GetGameInstance());
	verify(PurgatoryGameInstance);

	PurgatoryGameInstance->CompletedMap(bIsWinner);
}

void APurgatoryPlayerController::AddSpectatorPawn()
{
	if (!HasAuthority())
	{
		// Can only change state to spectator on the server side
		return;
	}

	UE_LOG(LogPlayer, Display, TEXT("%s: Changing state to spectator"), *GetName());

	ChangeState(NAME_Spectating);
	ClientGotoState(NAME_Spectating);
	HandleCameraAfterGameEnded();
}

void APurgatoryPlayerController::Spectate()
{
	UE_LOG(LogPlayer, Display, TEXT("%s: Spectate"), *GetName());

	// Allow the spectator pawn to take over the controls; otherwise, some of the bindings will be disabled
	DisableInput(this);

	AddSpectatorPawn();
}

void APurgatoryPlayerController::SetCameraOwnedBySpectatorPawn()
{
	auto MySpectatorPawn = GetSpectatorPawn();
	UE_LOG(LogPlayer, Display, TEXT("%s: Managing camera target with spectator pawn: %s"),
		*GetName(),
		(MySpectatorPawn ? *MySpectatorPawn->GetName() : TEXT("NULL"))
	);

	if (!MySpectatorPawn)
	{
		return;
	}

	OnSpectatorPawnActivated(MySpectatorPawn, PlayerPawn);

	AutoManageActiveCameraTarget(MySpectatorPawn);
}

void APurgatoryPlayerController::HandleCameraAfterGameEnded()
{
	if (PlayerPawn)
	{
		UE_LOG(LogPlayer, Display, TEXT("%s: HandleCameraAfterGameEnded: Tracking previous player pawn %s"),
			*GetName(),
			*PlayerPawn->GetName()
		);

		SetViewTarget(PlayerPawn);

		GetWorldTimerManager().SetTimer(SpectatorCameraDelayTimer,
			this, &APurgatoryPlayerController::SetCameraOwnedBySpectatorPawn, SpectatorCameraControlsDelay);

	}
	else
	{
		UE_LOG(LogPlayer, Warning, TEXT("%s: HandleCameraAfterGameEnded: Previous player pawn NULL - skipping directly to spectator controls"),
			*GetName()
		);

		SetCameraOwnedBySpectatorPawn();
	}
}