// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Quiz.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UQuiz : public UInterface
{
	GENERATED_BODY()
};

UENUM(BlueprintType)
enum class EQuizAnswerResult : uint8
{
	Fail UMETA(DisplayName = "Fail"),
	Correct UMETA(DisplayName = "Correct"),
	Incorrect UMETA(DisplayName = "Incorrect"),
	BadEntry UMETA(DisplayName = "BadEntry")
};

USTRUCT(BlueprintType)
struct FQuizAnswer
{
	GENERATED_BODY()

	FString Value {};
	FString Text{};
};

USTRUCT(BlueprintType)
struct FQuizQuestion
{
	GENERATED_BODY()

	int32 QuestionId{};
	FString QuestionText{};
	TArray<FQuizAnswer> Answers{};
	FString CorrectValue{};
};

/**
 * 
 */
class PURGATORY_API IQuiz
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual const FQuizQuestion* GetNextQuestion() = 0;
	virtual EQuizAnswerResult AnswerQuestion(int32 QuestionId, const FString& AnswerValue) = 0;
};
