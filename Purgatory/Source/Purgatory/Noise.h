#pragma once

#include "CoreMinimal.h"

namespace Noise
{
	struct FNoiseConfig
	{
		FName Tag{};
		float Loudness{};
		float MaxRange{};
	};

	inline const FNoiseConfig GunFire{ FName("GunFire"), 1.0f, 2000.0f };
	inline const FNoiseConfig Jump{ FName("Jump"), 0.5f, 600.0f };
	inline const FNoiseConfig Movement{ FName("Movement"), 0.5f, 600.0f };
}