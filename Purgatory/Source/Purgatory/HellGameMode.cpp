// Fill out your copyright notice in the Description page of Project Settings.


#include "HellGameMode.h"

#include "PurgatoryCharacter.h"

#include "PurgatoryGameStateBase.h"
#include "EngineUtils.h"
#include "Kismet/GameplayStatics.h" 
#include "GameFramework/Controller.h"
#include "PurgatoryCharacter.h"

void AHellGameMode::PawnKilled(APawn* PawnKilled)
{
	Super::PawnKilled(PawnKilled);

	if (!PawnKilled)
	{
		UE_LOG(LogGameMode, Warning, TEXT("%s : PawnKilled - Passed in NULL for PawnKilled"), *GetName());
		return;
	}

	auto State = GetGameState<APurgatoryGameStateBase>();

	if (!State)
	{
		UE_LOG(LogGameMode, Error, TEXT("%s : PawnKilled - GameState is not APurgatoryGameStateBase"), *GetName());
		return;
	}

	auto PlayerPawn = Cast<APurgatoryCharacter>(PawnKilled);

	// Player died
	if (PlayerPawn)
	{
		EndGame(false);
	}
	else if (auto EnemyAICount = State->GetPotentialEnemyAICount(); !EnemyAICount)
	{
		EndGame(true);
	}
	else
	{
		UE_LOG(LogGameMode, Log, TEXT("%s: Enemies remaining: %d"), *GetName(), EnemyAICount);
	}
}