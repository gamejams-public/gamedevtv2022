// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawner.h"

#include "Components/ArrowComponent.h" 
#include "Logging.h"

namespace
{
	constexpr float FloorTestExtent = 300.0f;
}

AEnemySpawner::AEnemySpawner()
{
	PrimaryActorTick.bCanEverTick = false;

	// Arrow components only visible in the editor and end up not existing in game mode at all (even to capture spawn locations!)
	// So need another root component to capture this
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnPoint"));
	SpawnPoint->SetupAttachment(GetRootComponent());
}

TSubclassOf<AActor> AEnemySpawner::GetActorSpawnClass() const
{
	auto NumSpawnClasses = ActorSpawnClasses.Num();
	ensureMsgf(NumSpawnClasses, TEXT("Actor Spawn Classes is not empty"));

	if (!NumSpawnClasses)
	{
		return {};
	}

	return ActorSpawnClasses[FMath::RandRange(0, ActorSpawnClasses.Num() - 1)];
}

AActor* AEnemySpawner::SpawnActor() const
{
	const FVector& SpawnLocation = GetActorLocation() + FVector(0.0f, 0.0f, SpawnZOffset);
	const FRotator& SpawnRotation = GetActorRotation();

	UWorld* World = GetWorld();
	if (!World)
	{
		UE_LOG(LogAI, Error, TEXT("%s: ToSpawn is nullptr  unable to spawn a pawn at location=%s"), *GetName(), *SpawnLocation.ToCompactString());
		return nullptr;
	}

	auto ActorSpawnClass = GetActorSpawnClass();

	if (!ActorSpawnClass)
	{
		UE_LOG(LogAI, Warning, TEXT("%s: Has no actor class set - no spawning will occur at %s"), *GetName(), *SpawnLocation.ToCompactString());
		return nullptr;
	}

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

	auto SpawnedActor = World->SpawnActor<AActor>(ActorSpawnClass, SpawnLocation, SpawnRotation, SpawnParams);

	if (SpawnedActor && bSnapToFloor)
	{
		SnapSpawnedActorToFloor(SpawnedActor);
	}
	else if(!SpawnedActor)
	{
		UE_LOG(LogAI, Warning, TEXT("%s: Unable to spawn actor %s at Location=%s with Rotation=%s"),
			*GetName(), *ActorSpawnClass->GetName(), *SpawnLocation.ToCompactString(), *SpawnRotation.ToCompactString());
	}

	return SpawnedActor;
}

void AEnemySpawner::SnapSpawnedActorToFloor(AActor* SpawnedActor) const
{
	auto SpawnLocation = SpawnedActor->GetActorLocation();

	FHitResult HitResult;

	// Avoid self-colliding
	FCollisionQueryParams CollisionQueryParams;
	CollisionQueryParams.AddIgnoredActor(SpawnedActor);

	bool bHit = GetWorld()->LineTraceSingleByChannel(
		HitResult,
		SpawnLocation,
		SpawnLocation - FVector(0.0f, 0.0f, FloorTestExtent),
		ECollisionChannel::ECC_Visibility,
		CollisionQueryParams);

	if (bHit)
	{
		SpawnLocation.Z = HitResult.Location.Z;
	}

	FVector ActorOrigin;
	FVector BoxExtent;

	SpawnedActor->GetActorBounds(true, ActorOrigin, BoxExtent, false);

	SpawnLocation.Z += BoxExtent.Z;

	SpawnedActor->SetActorLocation(SpawnLocation);
}


