// Copyright Epic Games, Inc. All Rights Reserved.

#include "PurgatoryGameMode.h"

#include "PurgatoryCharacter.h"
#include "UObject/ConstructorHelpers.h"

#include "PurgatoryGameStateBase.h"
#include "EngineUtils.h"
#include "Kismet/GameplayStatics.h" 
#include "GameFramework/Controller.h"
#include "PurgatoryCharacter.h"
#include "RighteousMeterComponent.h"
#include "PurgatoryEnemyCharacter.h"
#include "EnemySpawner.h"

#include <random>
#include <chrono>
#include <algorithm>

namespace
{
	constexpr float APurgatoryGameModeDelay = 1.0f;
}

APurgatoryGameMode::APurgatoryGameMode()
	: Super()
{

}

void APurgatoryGameMode::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(InitTimerHandle, this, &APurgatoryGameMode::InitAfterBeginPlay, APurgatoryGameModeDelay);
}

void APurgatoryGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GetWorldTimerManager().ClearTimer(SpawnTimerHandle);

	Super::EndPlay(EndPlayReason);
}

void APurgatoryGameMode::PawnKilled(APawn* PawnKilled)
{
	Super::PawnKilled(PawnKilled);

	if (!PawnKilled)
	{
		UE_LOG(LogGameMode, Warning, TEXT("%s : PawnKilled - Passed in NULL for PawnKilled"), *GetName());
		return;
	}

	auto State = GetPurgatoryGameState("PawnKilled");

	if (!State)
	{
		return;
	}

	auto PlayerPawn = Cast<APurgatoryCharacter>(PawnKilled);

	// Player died
	if (PlayerPawn)
	{
		EndGame(DetermineWonFromRighteousValue(PlayerPawn));
	}
	else if (auto EnemyAICount = State->GetPotentialEnemyAICount(); !EnemyAICount)
	{
		// TODO: Use a spawn actor to determine spawn points for new enemies and for every kill, spawn 2 more enemies - guarantee there is at least always one IsOrWillBecomeABadGuy
		// Player "wins" by dying with a positive value so this should never happen
		auto PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		EndGame(DetermineWonFromRighteousValue(Cast<APurgatoryCharacter>(PlayerController->GetPawn())));
	}
	else
	{
		UE_LOG(LogGameMode, Log, TEXT("%s: Enemies remaining: %d"), *GetName(), EnemyAICount);
	}
}

bool APurgatoryGameMode::DetermineWonFromRighteousValue(APurgatoryCharacter* PlayerPawn)
{
	check(PlayerPawn);

	auto Component = PlayerPawn->GetRighteousMeterComponent();
	check(Component);

	auto bRighteous = Component->IsRighteous();

	UE_LOG(LogGameMode, Log, TEXT("%s: Game ended with player righteous verdict of %s - value=%f"), *GetName(),
		bRighteous ? TEXT("TRUE") : TEXT("FALSE"),
		Component->GetRighteousPercent()
	);
	
	return bRighteous;
}

void APurgatoryGameMode::SpawnEnemy()
{
	ShuffleSpawnPointsAsNeeded();

	if (CurrentSpawnIndex >= EnemySpawnPoints.size())
	{
		UE_LOG(LogGameMode, Error, TEXT("%s: Unable to spawn enemy as the spawn points are empty: CurrentSpawnIndex=%d;NumSpawns=%d"),
			CurrentSpawnIndex, EnemySpawnPoints.size());
		return;
	}

	auto SpawnPoint = EnemySpawnPoints[CurrentSpawnIndex++].Get();
	if (!SpawnPoint)
	{
		UE_LOG(LogGameMode, Warning, TEXT("%s: SpawnPoint was GarbageCollected and no longer available! Skipping spawn"), *GetName());
		return;
	}

	auto SpawnedEnemy = Cast<APurgatoryEnemyCharacter>(SpawnPoint->SpawnActor());

	if (!SpawnedEnemy)
	{
		// already logged
		return;
	}

	SpawnedEnemy->SetBadGuyProbability(CurrentBadGuySpawnProbability, BadGuySpawnTurnMinTime, CurrentBadGuySpawnMaxTime);

	UE_LOG(LogGameMode, Log, TEXT("%s: Spawned enemy %s from spawn point %s at location %s"),
		*GetName(),
		*SpawnedEnemy->GetName(),
		*SpawnPoint->GetName(),
		*SpawnedEnemy->GetActorLocation().ToCompactString()
	);

	auto State = GetPurgatoryGameState("SpawnEnemy");

	if (State)
	{
		State->PawnAdded(SpawnedEnemy);
	}
}

void APurgatoryGameMode::TickSpawnEnemy()
{
	auto State = GetPurgatoryGameState("TickSpawnEnemy");

	if (!State)
	{
		return;
	}
	
	if (State->GetPotentialEnemyAICount() < MaxActiveEnemies)
	{
		SpawnEnemy();
	}
	else
	{
		UE_LOG(LogGameMode, Display, TEXT("%s: Spawn cycle disabled - at MaxActiveEnemies=%d"), MaxActiveEnemies);
	}

	// Schedule next cycle
	ScheduleNextSpawnTick();
}

void APurgatoryGameMode::ScheduleNextSpawnTick()
{
	auto World = GetWorld();
	if (!World)
	{
		UE_LOG(LogGameMode, Error, TEXT("%s: World is NULL"), *GetName());
		return;
	}

	// Check to see if we should increase rate
	const float CurrentTime = World->GetTimeSeconds();
	const float DeltaIncreaseTime = CurrentTime - LastSpawnRateIncreaseGameTime;

	// per minute increase up to maximums
	if (DeltaIncreaseTime >= 60)
	{
		CurrentSpawnRate = FMath::Min(CurrentSpawnRate + SpawnIncreaseRatePerMinute, MaxSpawnRatePerMinute);
		CurrentBadGuySpawnProbability = FMath::Min(CurrentBadGuySpawnProbability + BadGuySpawnProbabilityIncreasePerMinute, 1.0f);
		CurrentBadGuySpawnMaxTime = FMath::Max(BadGuySpawnTurnMinTime, CurrentBadGuySpawnMaxTime - BadGuySpawnTurnMaxTimeDecreasePerMinute);

		UE_LOG(LogGameMode, Display, TEXT("%s: Increased Difficulty - CurrentSpawnRate = %d/min; CurrentBadGuySpawnProbability=%f"),
			*GetName(),
			CurrentSpawnRate,
			CurrentBadGuySpawnProbability
		);

		LastSpawnRateIncreaseGameTime = CurrentTime;
	}

	const float NextSpawnDelay = 60.0f / CurrentSpawnRate;

	UE_LOG(LogGameMode, Verbose, TEXT("%s: Scheduling next spawn time in %f seconds"), *GetName(), NextSpawnDelay);

	// schedule next tick
	GetWorldTimerManager().SetTimer(SpawnTimerHandle, this, &APurgatoryGameMode::TickSpawnEnemy, NextSpawnDelay);
}

void APurgatoryGameMode::SpawnInitialEnemies()
{
	if (EnemySpawnPoints.empty())
	{
		UE_LOG(LogGameMode, Error, TEXT("%s: No enemy spawn points found!"), *GetName());
		return;
	}

	for (int i = 0; i < CurrentSpawnRate; ++i)
	{
		SpawnEnemy();
	}

	// schedule first tick
	ScheduleNextSpawnTick();
}

void APurgatoryGameMode::InitializeSpawnPoints()
{
	for (TObjectIterator<AEnemySpawner> Itr; Itr; ++Itr)
	{
		// Take first game world - only one instance should exist
		UWorld* InstanceWorld = Itr->GetWorld();

		//World Check to avoid getting objects from the editor world when doing PIE
		if (Itr->GetWorld() != GetWorld())
		{
			continue;
		}

		EnemySpawnPoints.push_back(*Itr);
	}

	CurrentSpawnIndex = 0;
	CurrentSpawnRate = InitialSpawnRatePerMinute;
	CurrentBadGuySpawnProbability = InitialBadGuySpawnProbability;
	CurrentBadGuySpawnMaxTime = InitialBadGuySpawnTurnMaxTime;

	ShuffleSpawnPointsAsNeeded(true);

	UE_LOG(LogGameMode, Display, TEXT("%s: NumEnemySpawnPoints=%d"),
		*GetName(),
		EnemySpawnPoints.size()
	);
}

void APurgatoryGameMode::InitAfterBeginPlay()
{
	UE_LOG(LogTemp, Log, TEXT("%s: InitAfterBeginPlay"), *GetName());

	InitializeSpawnPoints();
	SpawnInitialEnemies();
}

void APurgatoryGameMode::ShuffleSpawnPointsAsNeeded(bool bForceShuffle)
{
	if (!bForceShuffle && CurrentSpawnIndex < EnemySpawnPoints.size())
	{
		return;
	}

	UE_LOG(LogGameMode, Log, TEXT("%s: Shuffling spawn points"), *GetName());

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine rng(seed);

	// Clear out any reaped actors - use c++ erase, remove_if idiom
	EnemySpawnPoints.erase(
		std::remove_if(EnemySpawnPoints.begin(), EnemySpawnPoints.end(), [](const auto& WeakActor) { return !WeakActor.IsValid();  }),
		EnemySpawnPoints.end());

	std::shuffle(EnemySpawnPoints.begin(), EnemySpawnPoints.end(), rng);

	CurrentSpawnIndex = 0;
}
