// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_UpdatePerception.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "PurgatoryEnemyBlackboardKeys.h"
#include "Logging.h"

UBTService_UpdatePerception::UBTService_UpdatePerception()
{
	NodeName = "Update Perception";
}

void UBTService_UpdatePerception::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	auto BlackboardComponent = OwnerComp.GetBlackboardComponent();

	// key cleared - player no longer visible
	AActor* SeenPlayer = Cast<AActor>(BlackboardComponent->GetValueAsObject(PlayerSeenKey.SelectedKeyName));

	if(!SeenPlayer)
	{
		return;
	}

	BlackboardComponent->SetValueAsVector(PlayerSeenLocationKey.SelectedKeyName, SeenPlayer->GetActorLocation());
}
