// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "PurgatoryGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PURGATORY_API UPurgatoryGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	virtual void Init() override;

	UFUNCTION(BlueprintPure)
	FString GetProjectVersion() const;

	UFUNCTION(BlueprintCallable)
	void CompletedMap(bool bWon);

	UFUNCTION(BlueprintCallable)
	void QuitGame();

	UFUNCTION(BlueprintCallable)
	void RestartGame();

private:

	void LoadPurgatoryMap();
	void LoadHellMap();
	void LoadHeavenMap();
	void LoadMap(const FString& Name);
	void RestartMap();

private:
	UPROPERTY(Category = "Levels", EditDefaultsOnly)
	FString PurgatoryMapName;

	UPROPERTY(Category = "Levels", EditDefaultsOnly)
	FString HellMapName;
	
	UPROPERTY(Category = "Levels", EditDefaultsOnly)
	FString HeavenMap;
};
