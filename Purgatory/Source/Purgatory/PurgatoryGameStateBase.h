// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "PurgatoryGameStateBase.generated.h"

class APurgatoryEnemyCharacter;


UENUM(BlueprintType)
enum class EGameStage : uint8
{
	Purgatory UMETA(DisplayName = "Purgatory"),
	Hell UMETA(DisplayName = "Hell"),
	Heaven UMETA(DisplayName = "Heaven")
};

/**
 * 
 */
UCLASS(Abstract)
class PURGATORY_API APurgatoryGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:
	virtual void Init();
	virtual void PawnKilled(APawn* PawnKilled);
	virtual void PawnAdded(APawn* Pawn);

	UFUNCTION(BlueprintPure)
	int32 GetFriendlyAICount() const;

	UFUNCTION(BlueprintPure)
	int32 GetEnemyAICount() const;

	UFUNCTION(BlueprintPure)
	int32 GetPotentialEnemyAICount() const;

	EGameStage GetGameStage() const { return GameStage;  }

protected:

	UPROPERTY()
	TArray<APurgatoryEnemyCharacter*> ActiveCharacters;

	UPROPERTY(Category = "Stage", EditDefaultsOnly)
	EGameStage GameStage { EGameStage::Purgatory };
};
