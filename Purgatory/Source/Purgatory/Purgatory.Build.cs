// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Purgatory : ModuleRules
{
	public Purgatory(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { 
			"Core",
			"CoreUObject",
			"Engine",
			"InputCore",
			"HeadMountedDisplay",
			"GameplayTasks", // Blackboard and Behavior Tree
			"UMG", // Blueprint Widgets
			"Slate",
		});

		// Following https://forums.unrealengine.com/t/whats-needed-for-using-c-17/124529/2
		// also be sure to refresh the project in the unreal editor: Make sure to close VS and in UE4 do File -> Refresh Visual Studio project Files after this change
		CppStandard = CppStandardVersion.Cpp20;
	}
}
