// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BasePurgatoryGameMode.generated.h"

class APurgatoryGameStateBase;
/**
 * 
 */
UCLASS(Abstract)
class PURGATORY_API ABasePurgatoryGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	virtual void BeginPlay() override;

	virtual void PawnKilled(APawn* PawnKilled);

protected:
	virtual void EndGame(bool bIsPlayerWinner);

	APurgatoryGameStateBase* GetPurgatoryGameState(const char* LoggingFunctionName = "");

private:
	FTimerHandle InitTimerHandle{};
};
