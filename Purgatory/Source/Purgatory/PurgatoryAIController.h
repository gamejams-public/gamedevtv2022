// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"

#include "Perception/AIPerceptionTypes.h"
#include "Perception/AIPerceptionComponent.h"

#include "PurgatoryEnemyCharacter.h"

#include "PurgatoryAIController.generated.h"

class UBehaviorTree;
class UBlackboardComponent;
class APurgatoryEnemyCharacter;

/**
 * 
 */
UCLASS()
class PURGATORY_API APurgatoryAIController : public AAIController
{
	GENERATED_BODY()

public:
	APurgatoryAIController();

	//virtual void Tick(float DeltaTime) override;

	void DamagedByActor(AActor* Actor);

protected:
	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* InPawn) override final;

	FString GetPossessedPawnName() const;

	virtual void InstigatedAnyDamage(float Damage, const class UDamageType* DamageType, class AActor* DamagedActor, class AActor* DamageCauser) override;

private:
	void InitWithPawn(APurgatoryEnemyCharacter* InPawn);
	bool ShouldInitWithPawn(APawn* InPawn) const;

	void InitPerception();

	UFUNCTION()
	void OnPerceptionUpdated(const TArray<AActor*>& UpdatedActors);

	template<typename ActorClass>
	static ActorClass* GetFirstMatchingActorByClass(const TArray<AActor*>& Actors);

	template<typename SenseClass>
	const FAIStimulus* GetMatchingActiveStimulusByClass(const FActorPerceptionBlueprintInfo& PerceptionInfo) const;

private:
	UPROPERTY(EditDefaultsOnly)
	UBehaviorTree* AIBehavior {};

	UPROPERTY()
	APurgatoryEnemyCharacter* AIPawn {};

	// The team system is quite complicated - taking a shortcut here to just look for the pawn class of the player character vs other enemy classes
	/*
	* https://forums.unrealengine.com/t/how-do-i-use-the-ai-perception-teams/120837/2
	  https://www.thinkandbuild.it/ue4-ai-perception-system/
	*/
	//
	UPROPERTY()
	TArray<AActor*> CurrentlySeenActors;

	UPROPERTY()
	TArray<AActor*> CurrentlyHeardActors;
};

inline FString APurgatoryAIController::GetPossessedPawnName() const
{
	// TODO: Push this up to another AI base controller above this class in hierarchy
	return AIPawn ? *AIPawn->GetName() : TEXT("NULL");
}

template<typename ActorClass>
inline ActorClass* APurgatoryAIController::GetFirstMatchingActorByClass(const TArray<AActor*>& Actors)
{
	auto FoundActorResult = Actors.FindByPredicate([](auto& Actor)
	{
		return Cast<ActorClass>(Actor) != nullptr;
	});

	return FoundActorResult ? Cast<ActorClass>(*FoundActorResult) : nullptr;
}

template<typename SenseClass>
inline const FAIStimulus* APurgatoryAIController::GetMatchingActiveStimulusByClass(const FActorPerceptionBlueprintInfo& PerceptionInfo) const
{
	return PerceptionInfo.LastSensedStimuli.FindByPredicate([this](const auto& Stimulus)
	{
		return UAIPerceptionSystem::GetSenseClassForStimulus(this->GetWorld(), Stimulus) == SenseClass::StaticClass() &&
			Stimulus.IsActive();
	});
}
