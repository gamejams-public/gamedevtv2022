// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Damageable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UDamageable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class PURGATORY_API IDamageable
{
	GENERATED_BODY()

		// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void OnInit(float Health, float MaxHealth) = 0;
	virtual void OnAnyDamage(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator) = 0;
	virtual void OnGainedHealth(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator) = 0;
	virtual void OnHurt(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator) = 0;
	virtual void OnKilled(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator) = 0;
};
