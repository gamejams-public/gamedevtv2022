// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTService_UpdatePerception.generated.h"

/**
 * 
 */
UCLASS()
class PURGATORY_API UBTService_UpdatePerception : public UBTService
{
	GENERATED_BODY()

public:
	UBTService_UpdatePerception();

protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

private:
	UPROPERTY(Category = "Sight", EditAnywhere)
	FBlackboardKeySelector PlayerSeenKey {};

	UPROPERTY(Category = "Sight", EditAnywhere)
	FBlackboardKeySelector PlayerSeenLocationKey {};
};
