// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "BasePurgatoryGameMode.h"

#include <vector>

#include "PurgatoryGameMode.generated.h"

class APurgatoryCharacter;
class AEnemySpawner;

UCLASS(minimalapi)
class APurgatoryGameMode : public ABasePurgatoryGameMode
{
	GENERATED_BODY()

public:
	APurgatoryGameMode();

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void PawnKilled(APawn* PawnKilled);

private:
	bool DetermineWonFromRighteousValue(APurgatoryCharacter* PlayerPawn);


	UFUNCTION()
	void TickSpawnEnemy();

	void SpawnEnemy();
	void ShuffleSpawnPointsAsNeeded(bool bForceShuffle = false);
	void ScheduleNextSpawnTick();

	UFUNCTION()
	void InitAfterBeginPlay();

	void SpawnInitialEnemies();
	void InitializeSpawnPoints();

private:

	UPROPERTY(Category = "Difficulty", EditDefaultsOnly)
	int32 NumInitialSpawns { 10 };

	UPROPERTY(Category = "Difficulty", EditDefaultsOnly)
	int32 MaxActiveEnemies { 40 };

	UPROPERTY(Category = "Difficulty", EditDefaultsOnly)
	int32 InitialSpawnRatePerMinute { 10 };

	UPROPERTY(Category = "Difficulty", EditDefaultsOnly)
	float InitialBadGuySpawnProbability { 0.4f };

	UPROPERTY(Category = "Difficulty", EditDefaultsOnly)
	float BadGuySpawnProbabilityIncreasePerMinute { 0.2f };

	UPROPERTY(Category = "Difficulty", EditDefaultsOnly)
	float BadGuySpawnTurnMinTime{ 5.0f };

	UPROPERTY(Category = "Difficulty", EditDefaultsOnly)
	float InitialBadGuySpawnTurnMaxTime{ 30.0f };

	UPROPERTY(Category = "Difficulty", EditDefaultsOnly)
	float BadGuySpawnTurnMaxTimeDecreasePerMinute{ 10.0f };

	UPROPERTY(Category = "Difficulty", EditDefaultsOnly)
	int32 SpawnIncreaseRatePerMinute { 2 };

	UPROPERTY(Category = "Difficulty", EditDefaultsOnly)
	int32 MaxSpawnRatePerMinute { 20 };

	// Using a std::vector rather than TArray so we can use std::shuffle
	std::vector<TWeakObjectPtr<AEnemySpawner>> EnemySpawnPoints{};

	int32 CurrentSpawnRate{};
	float CurrentBadGuySpawnProbability{};
	float CurrentBadGuySpawnMaxTime{};
	std::size_t CurrentSpawnIndex{};

	float LastSpawnRateIncreaseGameTime{};

	FTimerHandle SpawnTimerHandle{};
	FTimerHandle InitTimerHandle{};
};



