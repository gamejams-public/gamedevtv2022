#pragma once

#include "CoreMinimal.h"

namespace KeyBindings
{
	inline const FName Pause = "Pause";
}