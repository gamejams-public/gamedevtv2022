// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePurgatoryGameMode.h"
#include "HellGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PURGATORY_API AHellGameMode : public ABasePurgatoryGameMode
{
	GENERATED_BODY()
	
public:

	virtual void PawnKilled(APawn* PawnKilled);
};
