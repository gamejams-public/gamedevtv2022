// Fill out your copyright notice in the Description page of Project Settings.


#include "PurgatoryEnemyCharacter.h"

#include "TP_WeaponComponent.h"
#include "Logging.h"
#include "Components/CapsuleComponent.h"

#include "PurgatoryAIController.h"

namespace
{
	constexpr float APurgatoryEnemyCharacterInitDelay = 0.2f;
}

// Sets default values
APurgatoryEnemyCharacter::APurgatoryEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	AgroOverlapVolume = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Agro Overlap Volume"));
	AgroOverlapVolume->SetupAttachment(GetRootComponent());
}

void APurgatoryEnemyCharacter::PlayFireWeaponAnimation(UAnimMontage* FireWeaponAnimationMontage)
{
	// TODO: Play correct montage for enemy
}

void APurgatoryEnemyCharacter::AttachWeapon(UTP_WeaponComponent* WeaponComponent)
{
	Super::AttachWeapon(WeaponComponent);

	if (!WeaponComponent)
	{
		UE_LOG(LogAI, Warning, TEXT("%s: Attempted to attach null UTP_WeaponComponent!"), *GetName());
		return;
	}

	// Attach the weapon to the First Person Character
	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
	WeaponComponent->GetOwner()->AttachToComponent(GetMesh(), AttachmentRules, FName(TEXT("GripPoint")));
}

// Called when the game starts or when spawned
void APurgatoryEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(InitTimerHandle, this, &APurgatoryEnemyCharacter::InitAfterBeginPlay, APurgatoryEnemyCharacterInitDelay);
}

void APurgatoryEnemyCharacter::OnHurt(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator)
{
	Super::OnHurt(CurrentHealth, PreviousHealth, DamageOwner, DamageCauser, DamageInstigator);

	PlaySoundAtActorLocation(EnemyHurtSound);

	// Fight back if not already a bad guy
	// Friendly fire is disabled so this will only fire if damaged by player but can check
	if (!HasWeapon() && DamageInstigator && DamageInstigator->IsPlayerController())
	{
		UE_LOG(LogAI, Log, TEXT("%s: Is equipping gun in self defense!"), * GetName());

		GetWorldTimerManager().ClearTimer(BadGuyFireTimerHandle);
		EquipStartingWeapon();
	}

	auto PurgatoryController = Cast<APurgatoryAIController>(GetController());
	if (PurgatoryController)
	{
		PurgatoryController->DamagedByActor(DamageCauser);
	}
}

void APurgatoryEnemyCharacter::OnKilled(float CurrentHealth, float PreviousHealth, AActor* DamageOwner, AActor* DamageCauser, AController* DamageInstigator)
{
	PlaySoundAtActorLocation(EnemyDeathSound);

	Super::OnKilled(CurrentHealth, PreviousHealth, DamageOwner, DamageCauser, DamageInstigator);
}

bool APurgatoryEnemyCharacter::ShouldTakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) const
{
	if (!Super::ShouldTakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser))
	{
		return false;
	}

	// Ignore friendly fire - DamageCauser could and really should be the gun not the pawn so look for the EventInstigator
	if (EventInstigator && !EventInstigator->IsPlayerController())
	{
		UE_LOG(LogAI, Log, TEXT("%s: Ignoring friendly fire from %s-%s"), *GetName(), *EventInstigator->GetPawn()->GetName(), *EventInstigator->GetName());
		return false;
	}

	return true;
}

void APurgatoryEnemyCharacter::BadGuyDecision()
{
	auto World = GetWorld();

	// already a bad guy or had to draw weapon in self defense
	if (!World || bBadGuy || HasWeapon())
	{
		GetWorldTimerManager().ClearTimer(BadGuyFireTimerHandle);
		return;
	}

	float InstantProbability = 1.0f / ((BadGuyTurnMaxTime - BadGuyTurnMinTime) + 1);
	bBadGuy = World->GetTimeSeconds() >= BadGuyTurnTimeSeconds;

	if (bBadGuy)
	{
		UE_LOG(LogAI, Display, TEXT("%s: is now a bad guy!"), *GetName());

		EquipStartingWeapon();
	}
}

void APurgatoryEnemyCharacter::InitAfterBeginPlay()
{
	UE_LOG(LogAI, Log, TEXT("%s: InitAfterBeginPlay"), *GetName());

	bWillBecomeBadGuy = FMath::FRand() <= BadGuyProbability;

	UE_LOG(LogAI, Display, TEXT("%s: will %s be a bad guy!"), *GetName(), (bWillBecomeBadGuy ? TEXT("") : TEXT("NOT")));

	if (bWillBecomeBadGuy)
	{
		BadGuyTurnTimeSeconds = FMath::RandRange(BadGuyTurnMinTime, BadGuyTurnMaxTime);

		GetWorldTimerManager().SetTimer(BadGuyFireTimerHandle, this, &APurgatoryEnemyCharacter::BadGuyDecision, 1.0f, true, BadGuyTurnMinTime);
	}
}
