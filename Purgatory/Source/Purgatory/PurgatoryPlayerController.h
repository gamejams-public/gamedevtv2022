// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PurgatoryPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class PURGATORY_API APurgatoryPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void GameHasEnded(AActor* EndGameFocus, bool bIsWinner) override final;

	UFUNCTION(BlueprintCallable)
	void HideHUD();

	UFUNCTION(BlueprintCallable)
	void ShowHUD();

	UFUNCTION(BlueprintCallable)
	void SetInputModeUI();

	UFUNCTION(BlueprintCallable)
	void SetInputModeGame();

	UFUNCTION(BlueprintCallable)
	void PauseGame();

	UFUNCTION(BlueprintCallable)
	void ResumeGame();

protected:
	virtual void InstigatedAnyDamage(float Damage, const class UDamageType* DamageType, class AActor* DamagedActor, class AActor* DamageCauser) override;
	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* InPawn) override;

	virtual void TriggerEndOfLevel(bool bIsWinner);

	void DisplayWidgetClass(TSubclassOf<class UUserWidget> WidgetClass);

	virtual void SetupInputComponent() override;

	virtual void OnSpectatorPawnActivated(APawn* MySpectatorPawn, APawn* PreviousPlayerPawn);


private:

	void RemoveCurrentWidget();

	void Spectate();
	void AddSpectatorPawn();
	void HandleCameraAfterGameEnded();
	void SetCameraOwnedBySpectatorPawn();

private:
	UPROPERTY(Category = "Timers", EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float RestartDelay{ 10.0f };

	UPROPERTY(Category = "Timers", EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float RetryAfterDeathDelay{ 10.0f };

	UPROPERTY(Category = "Timers", EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float SpectatorCameraControlsDelay{ 2.0f };

	UPROPERTY(Category = "UI", EditAnywhere)
	TSubclassOf<class UUserWidget> HUDClass{};

	UPROPERTY(Category = "UI", EditAnywhere)
	TSubclassOf<class UUserWidget> PauseMenuClass{};

	UPROPERTY()
	UUserWidget* CurrentWidget {};

	UPROPERTY()
	APawn* PlayerPawn {};

	FTimerHandle RestartTimer;
	FTimerHandle GameEndedDelayTimer;
	FTimerHandle SpectatorCameraDelayTimer;
};
