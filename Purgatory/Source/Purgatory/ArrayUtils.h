#pragma once

#include "CoreMinimal.h"

namespace PurgatoryGame
{
	template<typename T, typename ElementStringFunc>
	FString ToString(const TArray<T>& Array, ElementStringFunc Func);


	template<typename T>
	FString ToStringObjectElements(const TArray<T>& Array)
	{
		return ToString(Array, [](auto Elm) -> decltype(auto)
		{
			return Elm->GetName();
		});
	}
}

/// Template Definition //////
namespace PurgatoryGame
{
	template<typename T, typename ElementStringFunc>
	FString ToString(const TArray<T>& Array, ElementStringFunc Func)
	{
		FString Output;
		Output.Reserve(Array.Num() * 128);

		Output.AppendChar('[');

		for (int i = 0, Len = Array.Num(); i < Len; ++i)
		{
			const auto& Elm = Array[i];
			if (i > 0)
			{
				Output.AppendChar(',');
			}

			Output.Append(Func(Elm));
		}

		Output.AppendChar(']');

		return Output;
	}
}
