// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawner.generated.h"

UCLASS()
class PURGATORY_API AEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemySpawner();
	AActor* SpawnActor() const;

private:
	TSubclassOf<AActor> GetActorSpawnClass() const;
	void SnapSpawnedActorToFloor(AActor* SpawnedActor) const;
private:

	UPROPERTY(Category = "Spawning", VisibleAnywhere)
	class UArrowComponent* SpawnPoint{};

	UPROPERTY(Category = "Spawning", EditAnywhere)
	float SpawnZOffset{ 0.0f };

	UPROPERTY(Category = "Spawning", EditAnywhere)
	bool bSnapToFloor{ false };

	UPROPERTY(Category = "Spawning", EditAnywhere)
	TArray<TSubclassOf<AActor>> ActorSpawnClasses{};
};

